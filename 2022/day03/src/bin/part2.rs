use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn resolve_priority(character: char) -> i32 {
    let start_ascii_code_lowercase = 'a' as i32;
    let start_ascii_code_uppercase = 'A' as i32;
    let char_in_ascii_code = character as i32;

    if char_in_ascii_code < start_ascii_code_lowercase {
        return char_in_ascii_code - start_ascii_code_uppercase + 27;
    }
    char_in_ascii_code - start_ascii_code_lowercase + 1
}

fn resolve_unique_chars(rucksack: &str) -> HashSet<char> {
    let characters: HashSet<char> = rucksack.chars().collect();
    characters
}

fn resolve_badge(rucksack1: &str, rucksack2: &str, rucksack3: &str) -> char {
    let all_items = format!("{}{}{}", rucksack1, rucksack2, rucksack3);
    for item in resolve_unique_chars(&all_items) {
        if rucksack1.contains(item) && rucksack2.contains(item) && rucksack3.contains(item) {
            return item;
        }
    }
    panic!("no duplicates found!")
}

// fn calculate_score(rucksack: &str) -> i32 {
//     let duplicate_item = find_duplicate_char(rucksack);
//     resolve_priority(duplicate_item)
// }

fn main() {
    let mut total_score = 0;
    let mut elf_group: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                elf_group.push(input_line);
                if elf_group.len() == 3 {
                    let badge = resolve_badge(&elf_group[0], &elf_group[1], &elf_group[2]);
                    let prio = resolve_priority(badge);
                    total_score += prio;
                    elf_group.clear();
                }
            }
        }
    }

    println!("total score: {:?}", total_score);
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_priority() {
        assert_eq!(resolve_priority('a'), 1, "'a' should be 1");
        assert_eq!(resolve_priority('z'), 26, "'z' should be 26");
        assert_eq!(resolve_priority('A'), 27, "'A' should be 27");
        assert_eq!(resolve_priority('Z'), 52, "'Z' should be 52");
    }

    #[test]
    fn test_badge() {
        assert_eq!(resolve_badge("abpcdef","ghijklmnopqr","stuvwxpyz"), 'p', "answer must be 'p' because it is each of the strings");
    }

}