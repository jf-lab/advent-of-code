use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

fn parse_line(root_folder: String, dir_folder: String, line: String) -> (String, String, i32) {
    let dir_command = "dir ";
    if line.starts_with(dir_command) {
        return (root_folder, dir_folder, 0);
    }

    let cd_dot_dot_command = "$ cd ..";
    if line.starts_with(cd_dot_dot_command) {
        let last_dot = root_folder.rfind('.').unwrap();
        let (new_root_key, _) = root_folder.split_at(last_dot);
        return (new_root_key.to_string(), new_root_key.to_string(), 0);
    }

    let cd_command = "$ cd ";
    if line.starts_with(cd_command) {
        let new_root_key = format!("{}.{}", root_folder, line.replace(cd_command, ""));
        return (new_root_key.clone(), new_root_key, 0);
    }

    if line.starts_with("$ ls") {
        return (root_folder.clone(), root_folder, 0);
    }

    let mut split = line.chars();
    if split.next().unwrap().is_numeric() {
        let line_split: Vec<&str> = line.split(' ').collect();
        let size_file = line_split[0].parse::<i32>().unwrap();
        return (root_folder, dir_folder, size_file);
    }

    panic!("unexpected condition occurred [root_key; {} line: {}]", root_folder, line);
}

fn calculate_update(map_key_input: String, file_size: i32, map: HashMap<String, i32>) -> (String, i32) {
    let map_key = map_key_input.as_str();
    match map.get(map_key) {
        None => (map_key_input, file_size),
        Some(old_size) => (map_key_input, old_size + file_size),
    }
}

fn consolidate_directories(map: HashMap<String, i32>) -> HashMap<String, i32> {
    let mut map_including_sub_dirs: HashMap<String, i32> = HashMap::new();

    for key_in_map in map.keys() {
        for (key, file_size) in &map {
            if key.starts_with(key_in_map) {
                let file_size_to_add = file_size.clone();
                match map_including_sub_dirs.get(key_in_map) {
                    None => map_including_sub_dirs.insert(key_in_map.to_string(), file_size_to_add),
                    Some(old_value) => map_including_sub_dirs.insert(key_in_map.to_string(), old_value + file_size_to_add)
                };
            }
        }
    }
    map_including_sub_dirs
}

fn main() {
    let mut map: HashMap<String, i32> = HashMap::new();
    let mut root_key = "".to_string();
    let mut dir_folder = String::from("");

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                let (root_folder, new_dir_folder, i) = parse_line(root_key, dir_folder, input_line.trim().to_string());
                root_key = root_folder;
                dir_folder = new_dir_folder.clone();

                let (key, updated_value) = calculate_update(new_dir_folder, i, map.clone());
                map.insert(key, updated_value);
            }
        }
        let map_including_sub_dirs = consolidate_directories(map);

        let sum_of_all_smaller_dirs: i32 = map_including_sub_dirs.iter().map(|k| k.1).filter(|k| k < &&100000).sum();

        println!("sum of all smaller dirs: {:?}", sum_of_all_smaller_dirs);
    } else {
        panic!("no input.txt found!");
    }
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "$ cd /
                        $ ls
                        dir a
                        14848514 b.txt
                        8504156 c.dat
                        dir d
                        $ cd a
                        $ ls
                        dir e
                        29116 f
                        2557 g
                        62596 h.lst
                        $ cd e
                        $ ls
                        584 i
                        $ cd ..
                        $ cd ..
                        $ cd d
                        $ ls
                        4060174 j
                        8033020 d.log
                        5626152 d.ext
                        7214296 k";

        let mut map: HashMap<String, i32> = HashMap::new();
        let mut root_key = "".to_string();
        let mut dir_folder = String::from("");
        for line in input.lines() {
            let (root_folder, new_dir_folder, i) = parse_line(root_key, dir_folder, line.trim().to_string());
            root_key = root_folder;
            dir_folder = new_dir_folder.clone();

            let (key, updated_value) = calculate_update(new_dir_folder, i, map.clone());
            map.insert(key, updated_value);

        }

        let map_including_sub_dirs = consolidate_directories(map);
        println!("{:?}", map_including_sub_dirs);


        let sum_of_all_smaller_dirs: i32 = map_including_sub_dirs.iter().map(|k| k.1).filter(|k| k < &&100000).sum();

        println!("sum of all smaller dirs: {:?}", sum_of_all_smaller_dirs);

        assert_eq!(map_including_sub_dirs.get("./").unwrap().clone(), 48381165 as i32);
        assert_eq!(map_including_sub_dirs.get("./.a.e").unwrap().clone(), 584 as i32);
        assert_eq!(sum_of_all_smaller_dirs, 95437);
    }
}