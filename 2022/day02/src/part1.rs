use std::cmp::Ordering;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(PartialEq, Debug, Copy, Clone)]
enum Hand {
    Rock,
    Scissors,
    Paper,
}

impl Hand {
    fn value(&self) -> i32 {
        match &self {
            Hand::Rock => 1,
            Hand::Scissors => 3,
            Hand::Paper => 2,
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Hand::Rock => {
                match other {
                    Hand::Rock => Some(Ordering::Equal),
                    Hand::Scissors => Some(Ordering::Greater),
                    Hand::Paper => Some(Ordering::Less),
                }
            }
            Hand::Scissors => {
                match other {
                    Hand::Rock => Some(Ordering::Less),
                    Hand::Scissors => Some(Ordering::Equal),
                    Hand::Paper => Some(Ordering::Greater),
                }
            }
            Hand::Paper => {
                match other {
                    Hand::Rock => Some(Ordering::Greater),
                    Hand::Scissors => Some(Ordering::Less),
                    Hand::Paper => Some(Ordering::Equal),
                }
            }
        }
    }
}


fn calculate_score(other_hand: Hand, my_hand: Hand) -> i32 {
    let mut score = 0;

    if my_hand < other_hand {
        score = 0;
    }

    if my_hand == other_hand {
        score = 3;
    }

    if my_hand > other_hand {
        score = 6;
    }

    return score + my_hand.value();
}

fn translate_value(character: &str) -> Hand {
    if character.eq("A") || character.eq("X") {
        return Hand::Rock;
    }
    if character.eq("B") || character.eq("Y") {
        return Hand::Paper
    }

    if character.eq("C") || character.eq("Z") {
        return Hand::Scissors
    }
    panic!("unrecognized value");
}

fn read_all() {
    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                let mut input_values = input_line.split_whitespace();
                let other_hand = translate_value(input_values.next().unwrap());
                let my_hand = translate_value(input_values.next().unwrap());
                let score = calculate_score(other_hand, my_hand);

                total_score += score;
            }

        }
    }

    println!("total score: {:?}", total_score);
}

fn main() {
    read_all()
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_win() {
        assert_eq!(calculate_score(Hand::Rock, Hand::Paper), 8)
    }

    #[test]
    fn test_loose() {
        assert_eq!(calculate_score(Hand::Paper, Hand::Rock), 1)
    }

    #[test]
    fn test_draw() {
        assert_eq!(calculate_score(Hand::Scissors, Hand::Scissors), 6)
    }

    #[test]
    fn test_rock_scissors() {
        assert_eq!(calculate_score(Hand::Rock, Hand::Scissors), 3)
    }
}