use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(PartialEq, Debug, Copy, Clone)]
enum Hand {
    Rock,
    Scissors,
    Paper,
}

#[derive(PartialEq, Debug, Copy, Clone)]
enum GameResult {
    Lost,
    Draw,
    Win,
}

fn convert_hand(character: &str) -> Hand {
    if character.eq("A") {
        return Hand::Rock;
    }
    if character.eq("B") {
        return Hand::Paper;
    }
    if character.eq("C") {
        return Hand::Scissors;
    }

    panic!("input not recognized")
}

fn convert_game_result(character: &str) -> GameResult {
    if character.eq("X") {
        return GameResult::Lost;
    }

    if character.eq("Y") {
        return GameResult::Draw;
    }

    if character.eq("Z") {
        return GameResult::Win;
    }

    panic!("input not recognized")
}


fn calculate_score(other_hand: Hand, game_result: GameResult) -> i32 {
    let mut score;
    match game_result {
        GameResult::Lost => {
            score = 0;
            match other_hand {
                Hand::Rock => score += 3,      // my hand must be scissors to loose
                Hand::Scissors => score += 2,  // my hand must be paper in order to loose
                Hand::Paper => score += 1,     // my hand must be rock in order to loose
            }
        }
        GameResult::Draw => {
            score = 3;
            match other_hand {
                Hand::Rock => score += 1,
                Hand::Scissors => score += 3,
                Hand::Paper => score += 2,
            }

        }
        GameResult::Win => {
            score = 6;
            match other_hand {
                Hand::Rock => score += 2,      // my hand must be paper to win
                Hand::Scissors => score += 1,  // my hand must be rock in order to win
                Hand::Paper => score += 3,     // my hand must be scissors in order to win
            }
        }
    }
    return score;
}

fn main() {
    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                let mut input_values = input_line.split_whitespace();
                let hand = convert_hand(input_values.next().unwrap());
                let game_result = convert_game_result(input_values.next().unwrap());
                let score = calculate_score(hand, game_result);

                total_score += score;
            }

        }
    }

    println!("total score: {:?}", total_score);
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_first_round() {
        let hand = convert_hand("A");
        let game_result = convert_game_result("Y");
        assert_eq!(calculate_score(hand, game_result), 4)
    }

    #[test]
    fn test_second_round() {
        let hand = convert_hand("B");
        let game_result = convert_game_result("X");
        assert_eq!(calculate_score(hand, game_result), 1)
    }

    #[test]
    fn test_third_round() {
        let hand = convert_hand("C");
        let game_result = convert_game_result("Z");
        assert_eq!(calculate_score(hand, game_result), 7)
    }

}