use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn read_all() {
    if let Ok(lines) = read_lines("./input.txt") {
        let mut board_elves = HashSet::new();

        let mut number_of_elves = 0;
        let mut highest_calories = 0;
        let mut sum_of_calories = 0;
        for line in lines {
            if let Ok(input_line) = line {
                let calories = input_line.parse::<i32>();
                if calories.is_err() {
                    number_of_elves = number_of_elves + 1;
                    board_elves.insert(sum_of_calories);
                    if sum_of_calories > highest_calories {
                        highest_calories = sum_of_calories;
                    }
                    sum_of_calories = 0;
                    println!("next elf, highest calories: {:?}", highest_calories);
                }
                if calories.is_ok() {
                    let calories_result = calories.unwrap();
                    sum_of_calories += calories_result;
                    println!("{:?} {:?}", calories_result, sum_of_calories);
                }
            }
        }

        println!("number of elves: {:?}  highest_calories: {:?}", number_of_elves,  highest_calories);
        println!("board: {:?}", board_elves);
        let mut hash_vec: Vec<&i32> = board_elves.iter().collect();
        hash_vec.sort_by(|a, b| b.cmp(a));
        println!("vec: {:?}", hash_vec);
        println!("top3: {:?}", hash_vec[0] + hash_vec[1] + hash_vec[2])
    }
}

fn main() {
    read_all()
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

