use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

#[derive(Debug, Clone)]
struct Stack {
    crates: Vec<String>,
}

#[derive(Debug, Clone)]
struct Harbor {
    stacks: HashMap<String, Stack>,
}

impl Harbor {
    pub fn new() -> Self {
        Harbor { stacks: HashMap::new() }
    }

    fn add_stack(&mut self, letter: &str, stack_raw: Vec<&str>) {
        let mut stack = Stack::new();
        for crate_str in stack_raw {
            stack.add_crate(crate_str);
        }
        self.stacks.insert(letter.to_string(), stack);
    }

    fn move_stack(&mut self, number: i32, from: &str, to: &str) {
        let mut from_stack = self.stacks.get(from).cloned().unwrap();
        let mut to_stack = self.stacks.get(to).cloned().unwrap();

        to_stack = from_stack.move_crates(number, to_stack);

        self.stacks.insert(to.to_string(), to_stack);
        self.stacks.insert(from.to_string(), from_stack);
    }

    fn move_stack_with_command(&mut self, command: &str) {
        let split: Vec<&str> = command.split(' ').collect();
        self.move_stack(split[1].parse::<i32>().unwrap(), split[3], split[5]);
    }

    fn resolve_message(&self) -> String {
        let map_size = self.stacks.len() + 1;
        let mut str = "".to_string();
        for i in 1..map_size {
            let top_crate = self.stacks.get(i.to_string().as_str()).cloned().unwrap().crates.pop().unwrap();
            str = format!("{}{}", str, top_crate);
        }

        str
    }
}

impl Stack {
    pub const fn new() -> Self {
        Stack { crates: Vec::new() }
    }

    fn add_crate(&mut self, letter: &str) {
        self.crates.push(letter.to_string());
    }

    fn move_crates(&mut self, amount: i32, other: Stack) -> Stack {
        let mut new_stack = Stack::new();
        new_stack.crates = other.crates.clone();

        for _i in 0..amount {
            let crate_to_move = self.crates.pop().unwrap();
            new_stack.crates.push(crate_to_move);
        }

        new_stack
    }
}


fn main() {

    // Too lazy to convert input.txt If you want to run your input, you need to adapt code first.
    let mut harbor = Harbor::new();
    harbor.add_stack("1", ["G", "F", "V", "H", "P", "S"].to_vec());
    harbor.add_stack("2", ["G", "J", "F", "B", "V", "D", "Z", "M"].to_vec());
    harbor.add_stack("3", ["G", "M", "L", "J", "N"].to_vec());
    harbor.add_stack("4", ["N", "G", "Z", "V", "D", "W", "P"].to_vec());
    harbor.add_stack("5", ["V", "R", "C", "B"].to_vec());
    harbor.add_stack("6", ["V", "R", "S", "M", "P", "W", "L", "Z"].to_vec());
    harbor.add_stack("7", ["T", "H", "P"].to_vec());
    harbor.add_stack("8", ["Q", "R", "S", "N", "C", "H", "Z", "V"].to_vec());
    harbor.add_stack("9", ["F", "L", "G", "P", "V", "Q", "J"].to_vec());

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                if input_line.starts_with("move") {
                    harbor.move_stack_with_command(&input_line);
                }
            }
        }
    }

    println!("message: {:?}", harbor.resolve_message());

}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn move_crates() {
        let mut stack1 = Stack::new();
        stack1.add_crate("A");
        stack1.add_crate("B");

        let mut stack2 = Stack::new();
        stack2.add_crate("C");
        stack2.add_crate("D");

        stack2 = stack1.move_crates(2, stack2);

        println!("{:?} {:?}", stack1, stack2);

        assert_eq!(stack1.crates.is_empty(), true);
        assert_eq!(stack2.crates.last().unwrap(), "A")
    }

    #[test]
    fn test_example() {
        let mut harbor = Harbor::new();

        harbor.add_stack("1", ["Z", "N"].to_vec());
        harbor.add_stack("2", ["M", "C", "D"].to_vec());
        harbor.add_stack("3", ["P"].to_vec());

        println!("{:?}", harbor);
        harbor.move_stack(1, "2", "1");
        println!("{:?}", harbor);
        harbor.move_stack(3, "1", "3");
        println!("{:?}", harbor);
        harbor.move_stack(2, "2", "1");
        println!("{:?}", harbor);
        harbor.move_stack_with_command("move 1 from 1 to 2");
        println!("{:?}", harbor);

        assert_eq!(harbor.resolve_message(), "CMZ");
    }
}