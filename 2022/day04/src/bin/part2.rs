use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Copy, Clone)]
struct Range {
    lbound: i32,
    ubound: i32,
}

impl Range {

    fn range_overlaps(&self, other: Range) -> bool {
        let lbound_is_in_range = self.lbound <= other.ubound && self.lbound >= other.lbound;
        let ubound_is_in_range = self.ubound >= other.lbound && self.ubound <= other.ubound;
        lbound_is_in_range || ubound_is_in_range
    }
}

fn resolve_range(section_assignment: &str) -> Range {
    let section_range: Vec<&str> = section_assignment.split('-').collect();
    let lower_bound: i32 = section_range[0].parse().unwrap();
    let higher_bound: i32 = section_range[1].parse().unwrap();
    Range { lbound: lower_bound, ubound: higher_bound }
}


fn parse_line(line: String) -> bool {
    let range_elves: Vec<&str> = line.split(',').collect();
    let range1 = resolve_range(range_elves[0]);
    let range2 = resolve_range(range_elves[1]);

    range1.range_overlaps(range2) || range2.range_overlaps(range1)
}


fn main() {

    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                if parse_line(input_line) {
                    total_score += 1;
                }
            }
        }
    }

    println!("total score: {:?}", total_score);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_overlaps() {
        assert_eq!(parse_line(String::from("2-4,6-8")), false);
        assert_eq!(parse_line(String::from("2-3,4-5")), false);
        assert_eq!(parse_line(String::from("5-7,7-9")), true);
        assert_eq!(parse_line(String::from("2-8,3-7")), true);
        assert_eq!(parse_line(String::from("6-6,4-6")), true);
        assert_eq!(parse_line(String::from("2-6,4-8")), true);
    }


}