use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Copy, Clone)]
struct Range {
    lbound: i32,
    ubound: i32,
}

impl Range {
    fn is_in_range(&self, other: Range) -> bool {
        self.lbound >= other.lbound && self.ubound <= other.ubound
    }
}

fn resolve_range(section_assignment: &str) -> Range {
    let section_range: Vec<&str> = section_assignment.split('-').collect();
    let lower_bound: i32 = section_range[0].parse().unwrap();
    let higher_bound: i32 = section_range[1].parse().unwrap();
    Range { lbound: lower_bound, ubound: higher_bound }
}


fn parse_line(line: String) -> bool {
    let range_elves: Vec<&str> = line.split(',').collect();
    let range1 = resolve_range(range_elves[0]);
    let range2 = resolve_range(range_elves[1]);

    range1.is_in_range(range2) || range2.is_in_range(range1)
}


fn main() {

    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                if parse_line(input_line) {
                    total_score += 1;
                }
            }
        }
    }

    println!("total score: {:?}", total_score);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_not_in_range() {
        assert_eq!(parse_line(String::from("2-4,6-8")), false)
    }

    #[test]
    fn test_overlapping_range() {
        assert_eq!(parse_line(String::from("2-6,4-8")), false)
    }

    #[test]
    fn test_in_range() {
        assert_eq!(parse_line(String::from("2-6,2-6")), true);
        assert_eq!(parse_line(String::from("2-6,6-6")), true);
        assert_eq!(parse_line(String::from("2-6,3-4")), true);
        assert_eq!(parse_line(String::from("2-6,3-5")), true);
        assert_eq!(parse_line(String::from("2-6,2-2")), true);
    }

}