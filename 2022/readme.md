# [advent of code 2022](https://adventofcode.com/2022) 🎄🎅🧑‍🎄🦀
In 2022 I was thinking of a good reason to invest time to improve my [rust](https://www.rust-lang.org) experience.

A coworker mentioned [advent of code](https://adventofcode.com) and I had a good reason 😉!
So I decided to do the assignments for 2022 in rust.
When I have a problem with borrowing in rust, I will make that clear in a commit. So people can use this repo to see how some compiler errors can be resolved.

In these assignments I will also experiment with the tooling to learn how rust is used.

## my setup
I use a windows laptop (windows 11) and [clion](https://www.jetbrains.com/clion/) with a [rust plugin](https://www.jetbrains.com/rust/).

## my adventures

| day         | adventure                                                                                                                                                                                                                                                                      |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [01][day01] | Just got started, only used one function to implement the algorithm                                                                                                                                                                                                            |
| [02][day02] | I split the code, added enum and implemented a custom [PartialOrd][rust-partial-ord] for the enum. Added also tests for the given examples                                                                                                                                     |
| [03][day03] | Reduced the configuration for adding two binaries to the system (compare `Cargo.toml` between day02 and day03). In part2 I got a problem with the borrow checker, found the solution after reading [chapter 4 of the rust book][rustbook-ownership] (see git history).         |                                                                                                                                           |
| [04][day04] | Today I got a problem with borrowing and ownership. This problem could easily be solved with a clone of the struct. In large scale systems this solution could be problematic.                                                                                                 |
| [05][day05] | Also a problem with a moved value, this can be solved by using an immutable variable in the method signature. I cheated a bit by not implementing a read of all lines of the file, only the commands.                                                                          |
| [06][day06] | Day 6 went a lot smoother, the algorithm was smaller and the variables could remain immutable.                                                                                                                                                                                 |
| [07][day07] | Primarily it was very difficult to solve part 1, due to a lot of borrowing. This could be solved by isolating functionality in separate functions. Part 2 was easier to solve.                                                                                                 |
| [08][day08] | It was a nice puzzle to solve. Today I discovered that a [reverse range][reverse-range] is a different type in rust, that was not what I expected.                                                                                                                             |
| [09][day09] | The algorithm was very complex this time. I googled a bit and found a solution on github. I used this solution to learn more about rust.                                                                                                                                       |
| [10][day10] | Day 10 was quite fun to do, however a bunch of code was needed to solve the puzzle.                                                                                                                                                                                            |
| [11][day11] | On this day I had some trouble with the borrow checker again. The solution was to use a reference to map. I also used more functionality from the enum, which is nice concept in rust. Especially in combination with match. The solution was moved to tests, which is easier. |

[rust-partial-ord]: https://doc.rust-lang.org/std/cmp/trait.PartialOrd.html
[rustbook-ownership]: https://doc.rust-lang.org/book/ch04-00-understanding-ownership.html
[reverse-range]: https://doc.rust-lang.org/std/ops/struct.Range.html#method.rev

[day01]: https://adventofcode.com/2022/day/1
[day02]: https://adventofcode.com/2022/day/2
[day03]: https://adventofcode.com/2022/day/3
[day04]: https://adventofcode.com/2022/day/4
[day05]: https://adventofcode.com/2022/day/5
[day06]: https://adventofcode.com/2022/day/6
[day07]: https://adventofcode.com/2022/day/7
[day08]: https://adventofcode.com/2022/day/8
[day09]: https://adventofcode.com/2022/day/9
[day10]: https://adventofcode.com/2022/day/10
[day11]: https://adventofcode.com/2022/day/11

