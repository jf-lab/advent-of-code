use std::collections::BTreeMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

#[derive(Debug, Copy, Clone)]
enum Operation {
    Add(i64),
    Multiply(i64, i64),
    AddSelf(),
    MultiplySelf(i64),
}

impl Operation {
    fn make_it_smaller(worry_level: i64, prime_divider: i64) -> i64 {
        worry_level % prime_divider
    }

    fn apply(&self, worry_level: i64) -> i64 {
        match self {
            Operation::Add(x) => x + worry_level,
            Operation::Multiply(x, prime_divider) => Operation::make_it_smaller(x * worry_level, *prime_divider),
            Operation::AddSelf() => worry_level + worry_level,
            Operation::MultiplySelf(prime_divider) => Operation::make_it_smaller(worry_level * worry_level, *prime_divider),
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct Test {
    divisible: i64,
    next_monkey_when_true: char,
    next_monkey_when_false: char,
}

impl Test {
    fn test(&self, worry_level: i64) -> char {
        if worry_level % self.divisible == 0 {
            self.next_monkey_when_true
        } else {
            self.next_monkey_when_false
        }
    }
}

#[derive(Debug, Clone)]
struct Monkey {
    id: char,
    items: Vec<i64>,
    operation: Operation,
    test: Test,
    inspects: i64,
}

impl Monkey {
    fn catch(&self, item: i64) -> Monkey {
        let mut items_to_mutate = self.items.clone();
        items_to_mutate.push(item);
        Monkey {
            id: self.id,
            items: items_to_mutate,
            operation: self.operation,
            test: self.test,
            inspects: self.inspects,
        }
    }
    fn throw(&self) -> (Monkey, char, i64) {
        let (first, tail) = self.items.split_first().unwrap();
        let after_operation = self.operation.apply(*first);
        let new_monkey = Monkey {
            id: self.id.clone(),
            items: tail.to_vec(),
            operation: self.operation,
            test: self.test,
            inspects: self.inspects + 1,
        };
        (new_monkey, self.test.test(after_operation), after_operation)
    }
}

fn process_items(monkey_id: char, start_play: BTreeMap<char, Monkey>) -> BTreeMap<char, Monkey> {
    let mut end_play = start_play.clone();
    let monkey = start_play.get(&monkey_id).unwrap();

    let (updated_monkey, next_monkey_id, item) = monkey.throw();
    end_play.insert(updated_monkey.id, updated_monkey);

    let monkey_to_receive_item = start_play.get(&next_monkey_id).unwrap();
    let updated_monkey_receive = monkey_to_receive_item.catch(item);
    end_play.insert(updated_monkey_receive.id, updated_monkey_receive);

    end_play
}

fn play_game(rounds: i64, start_play: BTreeMap<char, Monkey>) -> BTreeMap<char, Monkey> {
    let mut end_play = start_play.clone();

    for _ in 0..rounds {
        for monkey_turn in start_play.keys() {
            let monkey = end_play.get(monkey_turn).unwrap();
            for _ in 0..monkey.items.len() {
                end_play = process_items(*monkey_turn, end_play);
            }
        }
    }

    end_play.clone()
}

fn calculate_score(play: BTreeMap<char, Monkey>) -> i64 {
    let mut scores: Vec<i64> = Vec::new();
    for (_, monkey) in play {
        scores.push(monkey.inspects);
    }
    scores.sort_by(|a, b| b.cmp(a));
    let first = scores.get(0).unwrap();
    let second = scores.get(1).unwrap();

    first * second
}

fn read_from_file(rounds: i64, name_file: &str) -> i64 {
    let mut monkey_id: char = 'q';
    let mut operation = Operation::Add(0);
    let mut test_divisor: i64 = 0;
    let mut test_true_throw: char = 'q';
    let mut test_false_throw: char = 'q';
    let mut items: Vec<i64> = Vec::new();

    let mut prime_divider: i64 = 1;

    let mut all_monkeys: BTreeMap<char, Monkey> = BTreeMap::new();
    if let Ok(lines) = read_lines(name_file) {
        for line in lines {
            if let Ok(input_line) = line {
                let test_header = "  Test: divisible by ";
                if input_line.starts_with(test_header) {
                    test_divisor = input_line.replace(test_header, "").parse::<i64>().unwrap();
                    prime_divider *= test_divisor;
                }
            }
        }
    }

    if let Ok(lines) = read_lines(name_file) {
        for line in lines {
            if let Ok(input_line) = line {
                if input_line.starts_with("Monkey ") {
                    let possible_monkey_id: Vec<char> = input_line.chars().collect();
                    monkey_id = possible_monkey_id[7].clone();
                }

                let starting_items_header = "  Starting items: ";
                if input_line.starts_with(starting_items_header) {
                    items = input_line.replace(starting_items_header, "")
                        .split(", ")
                        .map(|e| e.parse::<i64>().unwrap())
                        .collect();
                }

                let operation_header = "  Operation: new = ";
                if input_line.starts_with(operation_header) {
                    let number_as_string: String = input_line.chars()
                        .filter(|c| c.is_numeric())
                        .collect();
                    match number_as_string.parse::<i64>() {
                        Ok(number) => {
                            if input_line.contains("*") {
                                operation = Operation::Multiply(number, prime_divider);
                            }
                            if input_line.contains("+") {
                                operation = Operation::Add(number);
                            }
                        }
                        Err(_) => {
                            if input_line.contains("*") {
                                operation = Operation::MultiplySelf(prime_divider);
                            }
                            if input_line.contains("+") {
                                operation = Operation::AddSelf();
                            }
                        }
                    };
                }

                let test_header = "  Test: divisible by ";
                if input_line.starts_with(test_header) {
                    test_divisor = input_line.replace(test_header, "").parse::<i64>().unwrap();
                }

                let test_true_header = "    If true: throw to monkey ";
                if input_line.starts_with(test_true_header) {
                    let number_as_string: String = input_line.chars()
                        .filter(|c| c.is_numeric())
                        .collect();
                    test_true_throw = number_as_string.parse::<char>().unwrap();
                }

                let test_false_header = "    If false: throw to monkey ";
                if input_line.starts_with(test_false_header) {
                    let number_as_string: String = input_line.chars()
                        .filter(|c| c.is_numeric())
                        .collect();
                    test_false_throw = number_as_string.parse::<char>().unwrap();
                }

                // println!("*** {:?}", input_line);
                if input_line.trim().is_empty() {
                    let test = Test {
                        divisible: test_divisor,
                        next_monkey_when_true: test_true_throw,
                        next_monkey_when_false: test_false_throw,
                    };
                    let monkey = Monkey {
                        id: monkey_id,
                        items: items.clone(),
                        inspects: 0,
                        operation,
                        test,
                    };
                    // println!("{:?}", monkey);
                    all_monkeys.insert(monkey_id, monkey);
                }
            }
        }
    }

    let map = play_game(rounds, all_monkeys);

    for (c, m) in &map {
        println!("{:?} --- {:?} --- {:?} --- {:?}", c, m.id, m.inspects, m.items);
    }

    let total_score = calculate_score(map);
    println!("total score: {}", total_score);
    total_score
}

fn main() {
    println!("see tests for solution");
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_add_operation() {
        let ops1 = Operation::Add(4);
        assert_eq!(ops1.apply(5), 9);
    }

    #[test]
    fn test_multiply_operation() {
        let ops1 = Operation::Multiply(4, 1000);
        assert_eq!(ops1.apply(5), 20);
    }

    #[test]
    fn test_test_struct_when_false() {
        let test = Test {
            divisible: 11,
            next_monkey_when_true: 'a',
            next_monkey_when_false: 'b',
        };

        assert_eq!(test.test(2), 'b');
    }

    #[test]
    fn test_test_struct_when_true() {
        let test = Test {
            divisible: 11,
            next_monkey_when_true: 'a',
            next_monkey_when_false: 'b',
        };

        assert_eq!(test.test(220), 'a');
    }

    #[test]
    fn test_example() {
        // read_from_file(1, "./test-input.txt");
        // read_from_file(20, "./test-input.txt");
        // read_from_file(1000, "./test-input.txt");
        // read_from_file(2000, "./test-input.txt");
        // read_from_file(3000, "./test-input.txt");
        // read_from_file(4000, "./test-input.txt");
        // read_from_file(5000, "./test-input.txt");
        // read_from_file(6000, "./test-input.txt");
        // read_from_file(7000, "./test-input.txt");
        // read_from_file(8000, "./test-input.txt");
        // read_from_file(9000, "./test-input.txt");
        let total_score = read_from_file(10000, "./test-input.txt");
        assert_eq!(total_score, 2713310158);
    }

    #[test]
    fn test_solution() {
        let total_score = read_from_file(10000, "./input.txt");
        assert_eq!(total_score, 35270398814);
    }
}