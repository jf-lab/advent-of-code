use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

fn print_forrest(forrest: Vec<Vec<char>>) {
    for horizontal_row in forrest {
        println!("{:?}", horizontal_row);
    }
}

fn is_tree_visible(x: usize, y: usize, forrest: &Vec<Vec<char>>) -> bool {
    let vert_size = forrest.len();
    let horiz_size = forrest.get(0).unwrap().len();

    let tree = forrest.get(y).unwrap().get(x).unwrap();

    let mut left_visible = true;
    let mut right_visible = true;
    let mut top_visible = true;
    let mut bottom_visible = true;

    for x_in_line in 0..horiz_size {
        if x_in_line != x {
            let current_tree = forrest.get(y).unwrap().get(x_in_line).unwrap();
            if current_tree >= tree {
                if x_in_line < x {
                    left_visible = false;
                }
                if x_in_line > x {
                    right_visible = false;
                }
            }
        }
    }

    for y_in_line in 0..vert_size {
        if y_in_line != y {
            let current_tree = forrest.get(y_in_line).unwrap().get(x).unwrap();
            if current_tree >= tree {
                if y_in_line < y {
                    top_visible = false;
                }
                if y_in_line > y {
                    bottom_visible = false;
                }
            }
        }
    }

    top_visible || left_visible || bottom_visible || right_visible
}

fn count_visible_trees(forrest: Vec<Vec<char>>) -> i32 {
    let mut total_outer_tree_count = 0;
    let mut total_inner_tree_count = 0;


    let vert_size = forrest.len();
    let horz_size = forrest.get(0).unwrap().len();

    for y in 0..vert_size {
        for x in 0..horz_size {
            if y == 0 || x == 0 || y == vert_size - 1 || x == horz_size - 1 {
                total_outer_tree_count += 1;
            } else {
                if is_tree_visible(x, y, &forrest) {
                    total_inner_tree_count += 1;
                }
            }
        }
    }

    println!("total inner trees: {} / total outer trees: {}", total_inner_tree_count, total_outer_tree_count);

    total_inner_tree_count + total_outer_tree_count
}

fn main() {
    let mut forrest = Vec::new();
    let mut horizontal: Vec<char>;

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                horizontal = input_line.chars().collect();
                forrest.push(horizontal);
            }
        }
    }

    let all_visible_trees = count_visible_trees(forrest.clone());

    println!("all trees: {}", all_visible_trees);
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_example() {
        let input = String::from("30373\n25512\n65332\n33549\n35390\n");
        let mut forrest = Vec::new();
        let mut horizontal;

        for line in input.lines() {
            horizontal = line.chars().collect();
            forrest.push(horizontal);
        }

        let all_visible_trees = count_visible_trees(forrest);

        println!("all trees: {}", all_visible_trees);

        assert_eq!(all_visible_trees, 21);
    }
}