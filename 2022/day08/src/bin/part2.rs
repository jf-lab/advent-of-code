use std::fs::File;
use std::io;
use std::io::BufRead;
use std::iter::Rev;
use std::ops::Range;
use std::path::Path;
use crate::Direction::{Horizontal, Vertical};

enum Direction {
    Horizontal,
    Vertical,
}

fn determine_visibility_range_rev(x: usize, y: usize, direction: Direction, range: Rev<Range<usize>>, forrest: Vec<Vec<char>>) -> i32 {
    let tree = forrest.get(y).unwrap().get(x).unwrap();

    let mut number_of_trees = 0;

    match direction {
        Horizontal => {
            for x_in_line in range {
                number_of_trees += 1;
                let current_tree = forrest.get(y).unwrap().get(x_in_line).unwrap();
                if current_tree >= tree {
                    return number_of_trees;
                }
            }
        }
        Vertical => {
            for y_in_line in range {
                number_of_trees += 1;
                let current_tree = forrest.get(y_in_line).unwrap().get(x).unwrap();
                if current_tree >= tree {
                    return number_of_trees;
                }
            }
        }
    }

    number_of_trees
}

fn determine_visibility_range(x: usize, y: usize, direction: Direction, range: Range<usize>, forrest: Vec<Vec<char>>) -> i32 {
    let tree = forrest.get(y).unwrap().get(x).unwrap();

    let mut number_of_trees = 0;

    match direction {
        Horizontal => {
            for x_in_line in range {
                number_of_trees += 1;
                let current_tree = forrest.get(y).unwrap().get(x_in_line).unwrap();
                if current_tree >= tree {
                    return number_of_trees;
                }
            }
        }
        Vertical => {
            for y_in_line in range {
                number_of_trees += 1;
                let current_tree = forrest.get(y_in_line).unwrap().get(x).unwrap();
                if current_tree >= tree {
                    return number_of_trees;
                }
            }
        }
    }

    number_of_trees
}

fn calculate_scenic_score(x: usize, y: usize, forrest: Vec<Vec<char>>) -> i32 {
    let vert_size = forrest.len();
    let horiz_size = forrest.get(0).unwrap().len();

    let horiz_up = determine_visibility_range(x, y, Horizontal, (x + 1)..horiz_size, forrest.clone());
    let vert_up = determine_visibility_range(x, y, Vertical, (y + 1)..vert_size, forrest.clone());
    let horiz_down = determine_visibility_range_rev(x, y, Horizontal, (0..x).rev(), forrest.clone());
    let vert_down = determine_visibility_range_rev(x, y, Vertical, (0..y).rev(), forrest);

    horiz_up * vert_up * horiz_down * vert_down
}

fn find_highest_scenic_score(forrest: Vec<Vec<char>>) -> i32 {
    let vert_size = forrest.len();
    let horiz_size = forrest.get(0).unwrap().len();

    let mut highest_scenic_score = 0;

    for y in 1..(vert_size - 1) {
        for x in 1..(horiz_size - 1) {
            let scenic_score = calculate_scenic_score(x, y, forrest.clone());
            if scenic_score > highest_scenic_score {
                highest_scenic_score = scenic_score;
            }
        }
    }

    highest_scenic_score
}

fn main() {
    let mut forrest = Vec::new();
    let mut horizontal: Vec<char>;

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                horizontal = input_line.chars().collect();
                forrest.push(horizontal);
            }
        }
    }

    let scenic_score = find_highest_scenic_score(forrest);
    println!("highest scenic score: {}", scenic_score);
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_example1() {
        let input = String::from("30373\n25512\n65332\n33549\n35390\n");
        let mut forrest = Vec::new();
        let mut horizontal: Vec<char>;

        for line in input.lines() {
            horizontal = line.chars().collect();
            forrest.push(horizontal);
        }

        let scenic_score = calculate_scenic_score(2, 1, forrest);

        assert_eq!(scenic_score, 4);
    }

    #[test]
    fn test_example2() {
        let input = String::from("30373\n25512\n65332\n33549\n35390\n");
        let mut forrest = Vec::new();
        let mut horizontal: Vec<char>;

        for line in input.lines() {
            horizontal = line.chars().collect();
            forrest.push(horizontal);
        }

        let scenic_score = calculate_scenic_score(2, 3, forrest);

        assert_eq!(scenic_score, 8);
    }

    #[test]
    fn test_example3() {
        let input = String::from("30373\n25512\n65332\n33549\n35390\n");
        let mut forrest = Vec::new();
        let mut horizontal: Vec<char>;

        for line in input.lines() {
            horizontal = line.chars().collect();
            forrest.push(horizontal);
        }

        let scenic_score = find_highest_scenic_score(forrest);

        assert_eq!(scenic_score, 8);
    }
}