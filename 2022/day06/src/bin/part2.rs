use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn find_marker(marker_length: usize, line: &str) -> usize {
    let rest_of_line = line.clone();

    for position in 0..rest_of_line.len() {
        let first_chars = &rest_of_line[(0 + position)..(marker_length + position)];
        println!("{:?}", first_chars);
        let mut possible_marker: Vec<&str> = first_chars.split("").filter(|a| a != &"").collect();
        possible_marker.sort();
        println!("{:?}", possible_marker);
        possible_marker.dedup_by(|a, b| a == b);
        if possible_marker.len() == marker_length {
            return position + marker_length;
        }
    }


    0
}


fn main() {
    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                total_score += find_marker(14, &input_line);
            }
        }
    }

    println!("total score: {:?}", total_score);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_marker_length4() {
        assert_eq!(find_marker(4,"mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 7);
        assert_eq!(find_marker(4,"bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(find_marker(4,"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }

    #[test]
    fn test_marker_length14() {
        assert_eq!(find_marker(14,"mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 19);
        assert_eq!(find_marker(14,"bvwbjplbgvbhsrlpgdmjqwftvncz"), 23);
        assert_eq!(find_marker(14,"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 26);
    }
}