use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn find_marker(line: &str) -> usize {
    let mut rest_of_line = line.clone();

    for position in 0..rest_of_line.len() {
        let first_chars = &rest_of_line[(0 + position)..(4 + position)];
        println!("{:?}", first_chars);
        let mut possible_marker: Vec<&str> = first_chars.split("").filter(|a| a != &"").collect();
        possible_marker.sort();
        println!("{:?}", possible_marker);
        possible_marker.dedup_by(|a, b| a == b);
        if possible_marker.len() == 4 {
            return position + 4;
        }
    }


    3
}


fn main() {
    let mut total_score = 0;
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                total_score += find_marker(&input_line);
            }
        }
    }

    println!("total score: {:?}", total_score);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_overlaps() {
        assert_eq!(find_marker("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 7);
        assert_eq!(find_marker("bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(find_marker("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }
}