// use crate::part1;
mod part1;
use crate::part1::{read_lines, simulate_rope};

fn main() {
    let mut input: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                input.push(input_line);
                // do something with input_line
            }
        }
    }

    // write a message with result
    println!("count moves: {:?}", simulate_rope::<10>(input));
}