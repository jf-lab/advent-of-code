use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

use std::{collections::HashSet, str::FromStr};
use std::error::Error;

// inspired by the solution by https://github.com/AlexanderNenninger/AoC2022/blob/master/src/etc/solution.rs

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Move {
    Up(isize),
    Down(isize),
    Left(isize),
    Right(isize),
}

impl Move {
    fn get_inner(&self) -> isize {
        match self {
            Move::Up(n) => *n,
            Move::Down(n) => *n,
            Move::Left(n) => *n,
            Move::Right(n) => *n,
        }
    }
}

impl FromStr for Move {
    type Err = Box<dyn Error + Send + Sync + 'static>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (dir, n_steps) = s
            .split_once(" ")
            .ok_or("ERROR: Could not split move input.".to_string())?;
        let n_steps: isize = n_steps.parse()?;
        Ok(match dir {
            "U" => Self::Up(n_steps),
            "D" => Self::Down(n_steps),
            "L" => Self::Left(n_steps),
            "R" => Self::Right(n_steps),
            _ => Err("ERROR: Unknown direction identifier.".to_string())?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Rope<const N: usize> {
    knots: [(i64, i64); N],
}

impl<const N: usize> Rope<N> {
    fn new() -> Self {
        Rope { knots: [(0, 0); N] }
    }

    fn step_one(&mut self, m: Move) {
        match m {
            Move::Up(_) => self.knots[0].0 += 1,
            Move::Down(_) => self.knots[0].0 -= 1,
            Move::Left(_) => self.knots[0].1 -= 1,
            Move::Right(_) => self.knots[0].1 += 1,
        }
        self.drag_tail();
    }

    fn step(&mut self, m: Move, visited: &mut HashSet<(i64, i64)>) {
        for _ in 0..m.get_inner() {
            self.step_one(m);
            visited.insert(self.knots[N - 1]);
        }
    }

    fn drag_tail(&mut self) {
        for i in 1..N {
            let vdiff = self.knots[i - 1].0 - self.knots[i].0;
            let hdiff = self.knots[i - 1].1 - self.knots[i].1;

            if vdiff.abs() > 1 && hdiff.abs() > 0 {
                self.knots[i].0 += vdiff.signum();
                self.knots[i].1 += hdiff.signum();
            } else if vdiff.abs() > 0 && hdiff.abs() > 1 {
                self.knots[i].0 += vdiff.signum();
                self.knots[i].1 += hdiff.signum();
            } else if vdiff.abs() > 1 {
                self.knots[i].0 += vdiff.signum()
            } else if hdiff.abs() > 1 {
                self.knots[i].1 += hdiff.signum()
            }
        }
    }
}

pub fn simulate_rope<const K: usize>(input: Vec<String>) -> u64 {
    let mut visited = HashSet::new();
    let moves: Vec<Move> = input.iter()
        .map(|l| l.parse().expect("ERROR: Could not parse move."))
        .collect();
    let mut rope = Rope::<K>::new();
    visited.insert(rope.knots[K - 1]);

    for m in moves {
        rope.step(m, &mut visited)
    }

    visited.len() as u64
}

fn main() {
    let mut input: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                input.push(input_line);
                // do something with input_line
            }
        }
    }

    // write a message with result
    println!("count moves: {:?}", simulate_rope::<2>(input));
}


pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_example() {
        let steps = "R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2";
        let lines: Vec<String> = steps.lines().map(|e| e.to_string()).collect();
        let result = simulate_rope::<2>(lines);
        println!("{}", result);
        assert_eq!(result, 13);
    }
}