use std::collections::BTreeMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;
use crate::Command::{Addx, Noop};

#[derive(PartialEq)]
enum Command {
    Addx,
    Noop,
}

fn split_instruction(input: String) -> (Command, i32) {
    let split: Vec<String> = input.split(" ").map(|e| e.to_string()).collect();
    let command = split.get(0).unwrap();
    if command == "addx" {
        let mutation = split.get(1).unwrap().to_string().parse::<i32>();
        return (Addx, mutation.unwrap());
    }
    (Noop, 0)
}

fn calculate_total_signal_strength(map: BTreeMap<i32, i32>) -> i32 {
    let cycles = [20, 60, 100, 140, 180, 220];
    let mut total_mutation = 1;
    let mut total_strength = 0;

    let mut index_cycle = 0;

    for (click, mutation) in map {
        if click >= cycles[index_cycle] {
            let strength = cycles[index_cycle] * total_mutation;
            total_strength += strength;
            index_cycle += 1;
        }

        total_mutation += mutation;

        if index_cycle == cycles.len() {
            return total_strength;
        }
    }

    total_strength
}

fn create_stack(input: Vec<String>) -> BTreeMap<i32, i32> {
    let mut click = 0;

    let mut map = BTreeMap::new();
    for instruction in input {
        let (command, mutation) = split_instruction(instruction);
        click += 1;
        if command == Addx {
            click += 1;
            map.insert(click, mutation);
        }
    }

    map
}

fn main() {

    let mut input: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                input.push(input_line);
            }
        }
    }

    let map = create_stack(input);
    let total_signal_strength = calculate_total_signal_strength(map);

    println!("total strength: {:?}", total_signal_strength);
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn test_example() {
        let input = "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop";

        // let mut stack = Vec::new();
        let mut input_lines = input.lines().map(|e| e.to_string()).collect();
        let map = create_stack(input_lines);

        println!("Total signal strength: {}", calculate_total_signal_strength(map.clone()));
        assert_eq!(calculate_total_signal_strength(map), 13140);
    }
}