use std::collections::BTreeMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;
use crate::Command::{Addx, Noop};


fn render_line(line: Vec<bool>) -> String {
    let x: String = line.iter()
        .map(|e| if *e { '#' } else { '.' })
        .collect();

    x
}

fn create_sprite(number: usize) -> Vec<bool> {
    let mut vec = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

    if number <= 0 || number >= 40 {
        if number <= 0 {
            vec[0] = true;
        }
        if number >= 40 {
            vec[39] = true;
        }
        return vec.to_vec();
    }

    vec[number - 1] = true;
    vec[number] = true;
    vec[number + 1] = true;

    vec.to_vec()
}

#[derive(PartialEq)]
enum Command {
    Addx,
    Noop,
}

fn split_instruction(input: String) -> (Command, i32) {
    let split: Vec<String> = input.split(" ").map(|e| e.to_string()).collect();
    let command = split.get(0).unwrap();
    if command == "addx" {
        let mutation = split.get(1).unwrap().to_string().parse::<i32>();
        return (Addx, mutation.unwrap());
    }
    (Noop, 0)
}

fn create_stack(input: Vec<String>) -> BTreeMap<i32, i32> {
    let mut click = 0;
    let mut total_mutation = 1;

    let mut map = BTreeMap::new();
    for instruction in input {
        let (command, mutation) = split_instruction(instruction);
        click += 1;
        if command == Addx {
            click += 1;
            total_mutation += mutation;
            map.insert(click, total_mutation);
        }
    }

    map
}

fn render_screen(stack: BTreeMap<i32, i32>) {
    let mut sprite = create_sprite(1);
    for i in 0..240 {
        let sprite_i = i % 40;

        sprite = stack.get(&(i as i32)).map_or_else(|| sprite, |v| {
            return if v < &1 {
                create_sprite(0)
            } else {
                create_sprite(*v as usize)
            }
        });

        if sprite_i == 0 {
            println!();
        }

        let pixel = sprite.get(sprite_i).unwrap();
        if *pixel {
            print!("#");
        } else {
            print!(".");
        }
    }

    println!()
}

fn main() {
    let mut input: Vec<String> = Vec::new();

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(input_line) = line {
                input.push(input_line);
            }
        }
    }

    let map = create_stack(input);
    render_screen(map);
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;


    #[test]
    fn test_render_sprite() {
        let vec1 = create_sprite(1);
        assert_eq!(render_line(vec1), "###.....................................");
        let vec2 = create_sprite(16);
        assert_eq!(render_line(vec2), "...............###......................");
        let vec3 = create_sprite(5);
        assert_eq!(render_line(vec3), "....###.................................");
    }

    #[test]
    fn test_example() {
        let input = "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop";

        // let mut stack = Vec::new();
        let input_lines = input.lines().map(|e| e.to_string()).collect();
        let map = create_stack(input_lines);

        render_screen(map);
    }
}