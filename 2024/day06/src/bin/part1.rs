use crate::Direction::{Down, Right};
use clap::Parser;
use log::{debug, info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use std::{fs, ops};
use time::macros::format_description;
use Direction::{Left, Up};

fn calculate(input: String) -> usize {
    TermLogger::init(
        LevelFilter::Info,
        ConfigBuilder::new()
            .set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]"))
            .build(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();

    info!("start calc result");
    let mut map: Vec<Vec<char>> = Vec::new();

    let y_max = input.lines().count() as i32;
    let mut x_max = 0;

    // create map
    input.lines().for_each(|line| {
        x_max = line.len() as i32;
        let chars: Vec<char> = line.chars().collect();
        map.push(chars);
    });

    // find start position
    let mut cur_pos = find_start_position(&map).unwrap();
    let mut direction = Move::new(Up);
    cur_pos = cur_pos + direction;

    loop {
        if cur_pos.x < 0 || cur_pos.y < 0 || cur_pos.x >= x_max || cur_pos.y >= y_max {
            break;
        }
        if map[cur_pos.y as usize][cur_pos.x as usize] == '#' {
            cur_pos = cur_pos - direction;
            direction = direction.turn();
        } else {
            map[cur_pos.y as usize][cur_pos.x as usize] = 'X';
        }
        cur_pos = cur_pos + direction;
    }

    info!("end calc result");

    debug!("The full map: ");

    map.iter().for_each(|chars| debug!("{:?}", chars));

    map.iter()
        .map(|l| l.iter().filter(|l| l == &&'X').count())
        .sum()
}

fn find_start_position(map: &Vec<Vec<char>>) -> Result<Pos, &str> {
    for (y_pos, chars) in map.iter().enumerate() {
        for (x_pos, char) in chars.iter().enumerate() {
            if char == &'^' {
                return Ok(Pos {
                    x: x_pos as i32,
                    y: y_pos as i32,
                });
            }
        }
    }
    Err("No start position found")
}

#[derive(Debug, Copy, Clone)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

#[derive(Debug, Copy, Clone)]
struct Pos {
    x: i32,
    y: i32,
}

#[derive(Debug, Copy, Clone)]
struct Move {
    direction: Direction,
}


impl ops::Add<Move> for Pos {
    type Output = Pos;

    fn add(self, other: Move) -> Pos {
        Pos {
            x: self.x + other.next().x,
            y: self.y + other.next().y,
        }
    }
}

impl ops::Sub<Move> for Pos {
    type Output = Pos;

    fn sub(self, other: Move) -> Pos {
        Pos {
            x: self.x - other.next().x,
            y: self.y - other.next().y,
        }
    }
}

impl Move {
    fn new(direction: Direction) -> Self {
        Move { direction }
    }

    fn next(&self) -> Pos {
        match self.direction {
            Up => Pos { x: 0, y: -1 },
            Right => Pos { x: 1, y: 0 },
            Down => Pos { x: 0, y: 1 },
            Left => Pos { x: -1, y: 0 },
        }
    }

    fn turn(&self) -> Move {
        match self.direction {
            Up => Move { direction: Right },
            Right => Move { direction: Down },
            Down => Move { direction: Left },
            Left => Move { direction: Up },
        }
    }
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", calculate(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...";

        let result = calculate(input.to_string());
        assert_eq!(result, 41);
    }
}
