use clap::Parser;
use log::{debug, info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use std::fs;
use time::macros::format_description;

fn calculate(input: String) -> isize {
    TermLogger::init(
        LevelFilter::Info,
        ConfigBuilder::new()
            .set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]"))
            .build(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
        .unwrap();

    info!("start analysis");

    let total_calibration_result: isize = input
        .lines()
        .map(|l| {
            let split_test_from_remaining_numbers: Vec<&str> = l.split(": ").collect();
            let answer: isize = split_test_from_remaining_numbers[0]
                .parse::<isize>()
                .expect("not a value");

            let remaining_numbers: Vec<isize> = split_test_from_remaining_numbers[1]
                .split(" ")
                .map(|n| n.parse::<isize>().expect("not parseable data"))
                .collect();

            match check(answer, remaining_numbers) {
                true => {
                    debug!("{} --- ok", l);
                    return answer;
                }
                false => 0,
            }
        })
        .sum();

    info!("end analysis");
    total_calibration_result
}

fn check(answer: isize, numbers: Vec<isize>) -> bool {
    let operator_count = numbers.iter().count() - 1;
    let count_variants = 3i32.pow(operator_count as u32); // power of three

    for i in 0..count_variants {
        let mut sum_local = 0;
        let mut get_bit = 0; // always first add and then multiply
        for (index, num) in numbers.iter().enumerate() {
            if get_bit == 1 {
                sum_local = sum_local * num;
            }
            if get_bit == 0 {
                sum_local += num;
            }
            if get_bit == 2 {
                sum_local = format!("{}{}", sum_local, num)
                    .parse::<isize>()
                    .expect("not a value");
            }
            get_bit = (i / 3i32.pow(index as u32)) % 3;  // with "bit" shifting we are able to get all possibilities with
            // all operator permutations
        }
        if sum_local == answer {
            return true; // short-cut when value has been found
        }
    }

    false // if nothing is found then return false
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", calculate(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20";

        let result = calculate(input.to_string());
        assert_eq!(result, 11387);
    }
}
