use std::fs;
use clap::Parser;
use log::{info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use time::macros::format_description;

fn total_distance(input: String) -> isize {
    TermLogger::init(LevelFilter::Info, ConfigBuilder::new().set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]")).build(), TerminalMode::Mixed, ColorChoice::Auto).unwrap();

    info!("start calc distance");
    let mut left_list: Vec<isize> = Vec::new();
    let mut right_list: Vec<isize> = Vec::new();

    input.lines()
        .map(|line| line.split_once(" "))
        .for_each(|line| {
            let (left, right) = line.unwrap();
            let left_number: isize = left.parse().expect("left number couldn't be parsed");
            let right_number: isize = right.trim().parse().expect("right number couldn't be parsed");
            left_list.push(left_number);
            right_list.push(right_number);
        });

    left_list.sort();
    right_list.sort();

    let list_size = left_list.len();

    let mut sum_distances = 0;

    for i in 0..list_size {
        sum_distances += (left_list[i] - right_list[i]).abs();
    }

    info!("end calc distance");
    sum_distances
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", total_distance(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "3   4\n4   3\n2   5\n1   3\n3   9\n3   3";

        let result = total_distance(input.to_string());
        assert_eq!(result, 11);
    }
}

