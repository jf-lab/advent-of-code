use std::fs;
use clap::Parser;
use log::{debug, info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use time::macros::format_description;

fn calculate(input: String) -> usize {
    TermLogger::init(LevelFilter::Info, ConfigBuilder::new().set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]")).build(), TerminalMode::Mixed, ColorChoice::Auto).unwrap();

    info!("start parsing reports");

    let number_of_safe_reports = input.lines()
        .filter(|l| {
            let report: Vec<i16> = l.split(" ").map(|nstr| nstr.parse::<i16>().expect("number is not in range")).collect();
            debug!("report: {:?}", report);
            let is_safe = safe_report(report);
            debug!("safe report: {:?}", is_safe);
            is_safe
        }).count();

    info!("end parsing reports");
    number_of_safe_reports
}

fn levels_in_range(previous: i16, next: i16) -> bool {
    let delta = (previous - next).abs();
    delta >= 1 && delta <= 3
}

fn safe_report(levels: Vec<i16>) -> bool {
    let mut report_is_safe = true;
    let mut increasing = false;
    let mut decreasing = false;
    let mut previous = 0;
    for level in levels {
        if previous != 0 {
            if level > previous {
                increasing = true;
                if decreasing {
                    report_is_safe = false;
                }
            }
            if level < previous {
                decreasing = true;
                if increasing {
                    report_is_safe = false;
                }
            }

            if !levels_in_range(previous, level) {
                report_is_safe = false;
            }

        }
        previous = level;
    }
    report_is_safe
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", calculate(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "7 6 4 2 1\n1 2 7 8 9\n9 7 6 2 1\n1 3 2 4 5\n8 6 4 4 1\n1 3 6 7 9";

        let result = calculate(input.to_string());
        assert_eq!(result, 2);
    }

    #[test]
    fn test_safe_report() {
        let input: Vec<i16> = vec![7, 6, 4, 2, 1];
        assert_eq!(safe_report(input), true);
    }

    #[test]
    fn test_unsafe_report() {
        let input: Vec<i16> = vec![1, 3, 2, 4, 5];
        assert_eq!(safe_report(input), false);
    }
}

