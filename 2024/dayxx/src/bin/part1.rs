use std::fs;
use clap::Parser;
use log::{info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use time::macros::format_description;

fn calculate(input: String) -> isize {
    TermLogger::init(LevelFilter::Info, ConfigBuilder::new().set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]")).build(), TerminalMode::Mixed, ColorChoice::Auto).unwrap();

    info!("start calc result");

    let mut sum_distances = 0;

    info!("end calc result");
    sum_distances
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", calculate(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example() {
        let input = "3   4\n4   3\n2   5\n1   3\n3   9\n3   3";

        let result = calculate(input.to_string());
        assert_eq!(result, 0);
    }
}

