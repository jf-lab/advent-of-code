<#
    .SYNOPSIS
    Creates a fresh new rust project with files pupulated.


    .PARAMETER Day
    Input the day number. Prefix with a zero when it is smaller than 10. 


    .EXAMPLE
    PS> .\CreateNewDay.ps1 -Day 01

    .EXAMPLE
    PS> .\CreateNewDay.ps1 -Day 20
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [String]
    $Day
)

$template = "dayxx"
$newName = "day$Day"

Copy-Item -Path "dayxx" -Recurse -Destination "day$Day"

$curWorkDir = Get-Location
$newWorkDir = "$curWorkDir\$newName"

$fileToUpdate = "$newWorkDir\Cargo.toml"
$content = [System.IO.File]::ReadAllText($fileToUpdate).Replace($template, $newName)
[System.IO.File]::WriteAllText($fileToUpdate, $content)

Set-Location -Path $newWorkDir

