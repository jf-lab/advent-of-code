use std::fs;
use clap::Parser;
use log::{debug, LevelFilter};
use regex::Regex;
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};
use time::macros::format_description;

fn calculate(input: String) -> isize {
    TermLogger::init(LevelFilter::Debug, ConfigBuilder::new().set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]")).build(), TerminalMode::Mixed, ColorChoice::Auto).unwrap();

    parse_memory(input.replace("\n", ""))
}

fn parse_memory(input: String) -> isize {
    let skip_regex = Regex::new(r"don't\(\).+?do\(\)").unwrap();
    let mul_regex = Regex::new(r"mul\(\d*,\d*\)").unwrap();
    let xy_regex = Regex::new(r"mul\((?<x>\d*),(?<y>\d*)\)").unwrap();

    let filter_all_dont: String = skip_regex.split(input.as_str()).collect();
    let filter_dont: &str = match filter_all_dont.split_once("don't()") {
        None => filter_all_dont.as_str(),
        Some((filter_latest_dont, _)) => filter_latest_dont
    };

    let result: isize = mul_regex.find_iter(filter_dont).map(|m| m.as_str())
        .map(|mul| {
            debug!("{}", mul);
            let caps = xy_regex.captures(mul).unwrap();
            let x: isize = caps["x"].parse().expect("x should be number");
            let y: isize = caps["y"].parse().expect("y should be number");
            x * y
        })
        .sum();
    result
}

/// Executable to run part 1 of the puzzle
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// File path of puzzle input
    #[arg(short, long)]
    puzzle_input: String,
}

fn main() {
    let args = Args::parse();

    let input: String = fs::read_to_string(args.puzzle_input).expect("puzzle file not found");

    println!("Total calculated: {}", calculate(input));
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_example_2() {
        let input = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";
        assert_eq!(parse_memory(input.to_string()), 48);
    }
}

