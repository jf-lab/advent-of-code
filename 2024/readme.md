# [advent of code 2024](https://adventofcode.com/2024) 🎄🎅🧑‍🎄🦀
In 2024 I decided to return to rust again (like in 2022) [rust](https://www.rust-lang.org).

## my setup
I use a windows laptop (windows 11) and [RustRover IDE](https://www.jetbrains.com/rust/).
The examples can also be run on linux too. 

## how to test, build and run the code

```shell
# First go into the proper directory (e.g. day01)
cd day01

# Run the tests
cargo test

# Build the solution
cargo build --release

## On windows
# Run part 1 (first download your puzzle input and store it in a file)
.\target\release\part1 --puzzle-input .\puzzle-input-part1.txt

# Run part 2 (first download your puzzle input and store it in a file)
.\target\release\part2 --puzzle-input .\puzzle-input-part2.txt

## On linux
# Run part 1 (first download your puzzle input and store it in a file)
./target/release/part1 --puzzle-input ./puzzle-input-part1.txt

# Run part 2 (first download your puzzle input and store it in a file)
./target/release/part2 --puzzle-input ./puzzle-input-part2.txt
```

## my adventures

| day         | implementation              | adventure                                                                                          | left to improve |
|-------------|-----------------------------|----------------------------------------------------------------------------------------------------|-----------------|
| [01][day01] | [par1 and part2][day01impl] | I needed to read the problem twice to fully understand it.                                         |                 | 
| [02][day02] | [par1 and part2][day02impl] | Had problems to complete part2                                                                     |                 | 
| [03][day03] | [par1 and part2][day03impl] | Found the answer first by changing the data first for part2 and then fixed the code                |                 | 
| [06][day06] | [par1 and part2][day06impl] | Finished part 1, rust gives the option to override operators (which was useful in this case).      |                 | 
| [07][day07] | [par1 and part2][day07impl] | Started with bit shifting for part 1, in part 2 needed to change to a different method with base 3 |                 | 


[day01]: https://adventofcode.com/2024/day/1

[day01impl]: ./day01/src/bin

[day02]: https://adventofcode.com/2024/day/2

[day02impl]: ./day02/src/bin

[day03]: https://adventofcode.com/2024/day/3

[day03impl]: ./day03/src/bin

[day06]: https://adventofcode.com/2024/day/6

[day06impl]: day06/src/bin

[day07]: https://adventofcode.com/2024/day/7

[day07impl]: day07/src/bin
