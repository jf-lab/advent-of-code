﻿using NUnit.Framework;
using static _2023.day03.Day03;
using static _2023.day03.Day03.Part;
using static NUnit.Framework.Assert;

namespace _2023.day03;

public class Day03Tests
{

    [Test]
    public void TestParseLine()
    {
        var numbers = ParseNumbers("467..114..");
        Contains(new PartNumber(0, 2, 467), numbers);
        Contains(new PartNumber(5, 7, 114), numbers);
    }

    [Test]
    public void TestResolveSymbolIndexes()
    {
        var symbolIndexes = ResolveSymbolIndexes("...$.*....");
        Console.WriteLine($"list: {symbolIndexes.Count}");
        symbolIndexes.ForEach(Console.WriteLine);
        Contains(3, symbolIndexes);
        Contains(5, symbolIndexes);
    }

    [Test]
    public void TestDay03ExamplePart1()
    {
        List<String> lines = new()
        {
            "467..114..",
            "...*......",
            "..35..633.",
            "......#...",
            "617*......",
            ".....+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598.."
        };

        AreEqual(4361, ParseLines(lines, Part1));
    }

    [Test]
    public void TestDay03ExamplePart2()
    {
        List<string> lines = new()
        {
            "467..114..",
            "...*......",
            "..35..633.",
            "......#...",
            "617*......",
            ".....+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598.."
        };

        AreEqual(467835, ParseLines(lines, Part2));
    }

    [Test]
    public void TestDayReddit01Example()
    {
        List<String> lines = new()
        {
            "12.......*..",
            "+.........34",
            ".......-12..",
            "..78........",
            "..*....60...",
            "78.........9",
            ".5.....23..$",
            "8...90*12...",
            "............",
            "2.2......12.",
            ".*.........*",
            "1.1..503+.56"
        };

        AreEqual(925, ParseLines(lines, Part1));
    }
}