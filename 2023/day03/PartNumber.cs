namespace _2023.day03;

public record PartNumber(int StartPos, int EndPos, int Id)
{
    public bool AreInRange(IEnumerable<int> posSymbols)
    {
        return posSymbols.Any(IsInRange);
    }

    public bool IsInRange(int posSymbol)
    {
        return posSymbol >= StartPos - 1 && posSymbol <= EndPos + 1;
    }
}