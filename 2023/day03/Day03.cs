﻿namespace _2023.day03;

public class Day03(IReadOnlyList<string> lines, Day03.Part part)
{
    public enum Part
    {
        Part1,
        Part2
    }

    public static int ParseLines(List<string> lines, Part part)
    {
        return new Day03(lines, part).Execute();
    }
    
    public int Execute()
    {
        Dictionary<int, List<PartNumber>> allNumbers = new();
        var totalSumPartIds = 0;
        for (var i = 0; i < lines.Count; i++)
        {
            var line = lines[i];
            List<PartNumber> possiblePartNumbers = new();
            var partNumbersPreviousLine = new List<PartNumber>();
            var partNumbersNextLine = new List<PartNumber>();

            if (i is 0)
            {
                allNumbers.Add(i, ParseNumbers(lines[i]));
            }
            else
            {
                partNumbersPreviousLine = allNumbers[i - 1];
            }

            if (i + 1 < lines.Count)
            {
                partNumbersNextLine = ParseNumbers(lines[i + 1]);
                allNumbers.Add(i + 1, partNumbersNextLine);
            }

            var partNumbersCurrentLine = allNumbers[i];

            possiblePartNumbers.AddRange(partNumbersPreviousLine);
            possiblePartNumbers.AddRange(partNumbersCurrentLine);
            possiblePartNumbers.AddRange(partNumbersNextLine);

            totalSumPartIds += part switch
            {
                Part.Part1 => PartNumberSum(possiblePartNumbers, ResolveSymbolIndexes(line)),
                Part.Part2 => CalculateGearRatio(possiblePartNumbers, ResolveGearIndexes(line)),
                _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
            };
        }

        return totalSumPartIds;
    }

    private static int CalculateGearRatio(List<PartNumber> possiblePartNumbers, List<int> gearIndexes)
    {
        var totalSumPartIds = 0;
        foreach (var gearIndex in gearIndexes)
        {
            var partIds = possiblePartNumbers.Where(pn => pn.IsInRange(gearIndex)).Select(pn => pn.Id).ToList();
            var partIdProduct = 1;
            var adjacentNumbers = 0;
            foreach (var partId in partIds)
            {
                adjacentNumbers++;
                partIdProduct *= partId;
            }


            if (adjacentNumbers == 2)
            {
                totalSumPartIds += partIdProduct;
            }
        }

        return totalSumPartIds;

    }

    private static List<int> ResolveGearIndexes(string line)
    {
        return line.ToCharArray().Select((value, i) => (value, i)).Where(IsGear).Select(k => k.i).ToList();
        bool IsGear((char value, int i) tuple) => tuple.value == '*';
    }

    private static int PartNumberSum(IEnumerable<PartNumber> possiblePartNumbers, List<int> symbolIndexes)
    {
        var queryResult = possiblePartNumbers.Where(pn => pn.AreInRange(symbolIndexes)).Select(pn => pn.Id).ToList();
        var sumPartIds = queryResult.Sum();
        return sumPartIds;
    }

    public static List<int> ResolveSymbolIndexes(string line)
    {
        return line.ToCharArray().Select((value, i) => (value, i)).Where(IsSymbol).Select(k => k.i).ToList();

        bool IsSymbol((char value, int i) tuple) => !(char.IsDigit(tuple.value) || tuple.value == '.');
    }

    public static List<PartNumber> ParseNumbers(string line)
    {
        List<PartNumber> allNumbersInLine = new();
        var startPos = 0;
        int endPos;
        var previousCharIsDigit = false;
        var number = "";
        var cs = line.ToCharArray();
        for (var index = 0; index < cs.Length; index++)
        {
            var c = cs[index];
            if (char.IsDigit(c))
            {
                if (!previousCharIsDigit)
                {
                    startPos = index;
                }

                number += c;
                previousCharIsDigit = true;

                if (index + 1 == cs.Length)
                {
                    allNumbersInLine.Add(new PartNumber(startPos, index, int.Parse(number)));
                }
            }
            else
            {
                if (previousCharIsDigit)
                {
                    endPos = index - 1;
                    allNumbersInLine.Add(new PartNumber(startPos, endPos, int.Parse(number)));
                    number = "";
                    previousCharIsDigit = false;
                }
            }
        }
        
        

        return allNumbersInLine;
    }
}