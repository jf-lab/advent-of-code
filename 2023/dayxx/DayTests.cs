using NUnit.Framework;
using static _2023.Part;

namespace _2023.dayxx;

public class DayTests
{
    private const string InputFilePath = @".\dayxx\input.txt";

    [Test]
    [Ignore("Part 1 not implemented yet")]
    public void Part1Test()
    {
        var readAllLines = ReadLinesUtil.ReadAllLines(InputFilePath);
        Day day = new(readAllLines, Part1);
        Assert.AreEqual(0, day.Execute());
    }

    [Test]
    [Ignore("Part 2 not implemented yet")]
    public void Part2Test()
    {
        var readAllLines = ReadLinesUtil.ReadAllLines(InputFilePath);
        Day day = new(readAllLines, Part2);
        Assert.AreEqual(1, day.Execute());
    }

    [Test]
    public void Part1ExampleTest()
    {
        var input = """
                    INPUT
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(0, day.Execute());
    }

    [Test]
    public void Part2ExampleTest()
    {
        var input = """
                    INPUT
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(1, day.Execute());
    }
}