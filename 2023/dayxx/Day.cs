namespace _2023.dayxx;

public class Day(IEnumerable<string> lines, Part part) 
{
    public int Execute()
    {
        return (int)part;
    }
}