﻿using NUnit.Framework;
using static _2023.day02.Day02;
using static _2023.day02.Day02.Part;
using static NUnit.Framework.Assert;

namespace _2023.day02;

public class Day02Tests
{

    [Test]
    public void TestParseLine()
    {
        AreEqual(1,
            ResolveGameNumberWhenGameIsPossible("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"));
        AreEqual(2,
            ResolveGameNumberWhenGameIsPossible("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"));
        AreEqual(0,
            ResolveGameNumberWhenGameIsPossible(
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"));
        AreEqual(0,
            ResolveGameNumberWhenGameIsPossible(
                "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"));
        AreEqual(5,
            ResolveGameNumberWhenGameIsPossible("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"));
    }

    [Test]
    public void TestResolvePower()
    {
        AreEqual(48,
            ResolvePower("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"));
        AreEqual(12,
            ResolvePower("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"));
        AreEqual(1560,
            ResolvePower("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"));
        AreEqual(630,
            ResolvePower("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"));
        AreEqual(36,
            ResolvePower("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"));
    }
}