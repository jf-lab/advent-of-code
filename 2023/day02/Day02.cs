﻿using System.Text.RegularExpressions;

namespace _2023.day02;

public class Day02
{
    private const int RedAvailable = 12;
    private const int GreenAvailable = 13;
    private const int BlueAvailable = 14;

    public enum Part
    {
        Part1,
        Part2
    }

    public int SumOfGameIds { get; }

    public Day02(List<string> lines, Part part)
    {
        foreach (var line in lines)
        {
            SumOfGameIds += part switch
            {
                Part.Part1 => ResolveGameNumberWhenGameIsPossible(line),
                Part.Part2 => ResolvePower(line),
                _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
            };
        }
    }

    public static int ResolvePower(string line)
    {
        var redMax = 0;
        var blueMax = 0;
        var greenMax = 0;

        var splitGameFromCubes = line.Split(": ");
        var cubes = splitGameFromCubes[1];

        var cubesArray = Regex.Split(cubes, @",\s|;\s");
        foreach (var cube in cubesArray)
        {
            var numberColorSplit = cube.Split(" ");
            var count = int.Parse(numberColorSplit[0]);

            if (numberColorSplit[1].Equals("red") && count > redMax)
            {
                redMax = count;
            }

            if (numberColorSplit[1].Equals("blue") && count > blueMax)
            {
                blueMax = count;
            }

            if (numberColorSplit[1].Equals("green") && count > greenMax)
            {
                greenMax = count;
            }
        }

        return redMax * blueMax * greenMax;
    }

    public static int ResolveGameNumberWhenGameIsPossible(string line)
    {
        var splitGameFromCubes = line.Split(": ");
        var gameId = splitGameFromCubes[0];
        var cubes = splitGameFromCubes[1];

        var cubesArray = Regex.Split(cubes, @",\s|;\s");
        foreach (var cube in cubesArray)
        {
            var numberColorSplit = cube.Split(" ");
            var count = int.Parse(numberColorSplit[0]);
            if (numberColorSplit[1].Equals("red") && count > RedAvailable)
            {
                return 0;
            }

            if (numberColorSplit[1].Equals("blue") && count > BlueAvailable)
            {
                return 0;
            }

            if (numberColorSplit[1].Equals("green") && count > GreenAvailable)
            {
                return 0;
            }
        }

        return int.Parse(gameId.Split(" ")[1]);
    }
}