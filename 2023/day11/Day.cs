namespace _2023.day11;

public class Day(List<string> lines, Part part)
{
    private const char EmptySpaceChar = '.';
    private const char GalaxyChar = '#';
    private List<Galaxy> _galaxies = [];
    private int _emptySpaceIncrementor;

    public record Galaxy(int X, int Y)
    {
        public int CalculateDistance(Galaxy other)
        {
            var xDistance = Math.Abs(X - other.X);
            var yDistance = Math.Abs(Y - other.Y);
            return xDistance + yDistance;
        }

        public override string ToString()
        {
            return $"Galaxy[{nameof(X)}: {X}, {nameof(Y)}: {Y}]";
        }
    };

    public long Execute(int emptySpaceIncrementor)
    {
        _emptySpaceIncrementor = emptySpaceIncrementor - 1;

        _galaxies = FindGalaxies();

        return FindAllCombinations()
            .Aggregate<(Galaxy a, Galaxy b), long>(0, (current, combination) => current + combination.a.CalculateDistance(combination.b));
    }

    public long Execute()
    {
        return part switch
        {
            Part.Part1 => Execute(2),
            Part.Part2 => Execute(1_000_000),
            _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
        };
    }

    public List<(Galaxy a, Galaxy b)> FindAllCombinations()
    {
        List<(Galaxy a, Galaxy b)> result = [];
        for (var i = 0; i < _galaxies.Count; i++)
        {
            var startGalaxy = _galaxies[i];
            result.AddRange(_galaxies.GetRange(i + 1, _galaxies.Count - (i + 1))
                .Select(galaxy => (startGalaxy, galaxy)));
        }

        return result;
    }


    public List<Galaxy> FindGalaxies()
    {
        var galaxies = new List<Galaxy>();
        var xEmptySpaceCounter = 0;
        var yEmptySpaceCounter = 0;

        var xEmptySpace = new List<int>();
        var yEmptySpace = new List<int>();

        for (var x = 0; x < lines[0].Length; x++)
        {
            var isEmpty = true;
            foreach (var line in lines)
            {
                if (!line[x].Equals(EmptySpaceChar))
                {
                    isEmpty = false;
                }
            }

            if (isEmpty)
            {
                xEmptySpaceCounter += _emptySpaceIncrementor;
            }

            xEmptySpace.Add(xEmptySpaceCounter);
        }

        foreach (var line in lines)
        {
            if (line.ToCharArray().All(a => a.Equals(EmptySpaceChar)))
            {
                yEmptySpaceCounter += _emptySpaceIncrementor;
            }

            yEmptySpace.Add(yEmptySpaceCounter);
        }

        for (var y = 0; y < lines.Count; y++)
        {
            for (var x = 0; x < lines[0].Length; x++)
            {
                if (lines[y][x].Equals(GalaxyChar))
                {
                    galaxies.Add(new Galaxy(xEmptySpace[x] + x, yEmptySpace[y] + y));
                }
            }
        }

        galaxies.ForEach(Console.WriteLine);

        return galaxies;
    }
}