using NUnit.Framework;
using static _2023.Part;

namespace _2023.day11;

public class DayTests
{
    
    private List<string> inputExample1 = """
            ...#......
            .......#..
            #.........
            ..........
            ......#...
            .#........
            .........#
            ..........
            .......#..
            #...#.....
            """.Split(Environment.NewLine).ToList();
    
    [Test]
    public void Part1ExampleFindGalaxiesTest()
    {
        Day day = new(inputExample1, Part1);
        day.Execute();

        var galaxies = day.FindGalaxies();
        Assert.AreEqual(9, galaxies.Count);
    }
    
    [Test]
    public void Part1ExampleFindAllCombinationsTest()
    {
        Day day = new(inputExample1, Part1);
        day.Execute();

        var galaxies = day.FindAllCombinations();
        Assert.AreEqual(36, galaxies.Count);
    }
    
    [Test]
    public void Part1ExampleTest()
    {
        Day day = new(inputExample1, Part1);
        Assert.AreEqual(374, day.Execute());
    }

    [Test]
    public void Part2ExampleStep10Test()
    {
        Day day = new(inputExample1, Part1);
        Assert.AreEqual(1030, day.Execute(10));
    }

    [Test]
    public void Part2ExampleStep100Test()
    {
        Day day = new(inputExample1, Part1);
        Assert.AreEqual(8410, day.Execute(100));
    }

}