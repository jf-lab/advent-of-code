using NUnit.Framework;

namespace _2023.day06;

public class DayTests
{
   
    [Test]
    public void Part1ExampleTest()
    {
        var lines = new List<string>
        {
            "Time:      7  15   30",
            "Distance:  9  40  200"
        };
        
        Assert.AreEqual(Int128.Parse("288"), new Day(lines).Execute(Part.Part1));
    }
    
    [Test]
    public void Part2ExampleTest()
    {
        var lines = new List<string>
        {
            "Time:      7  15   30",
            "Distance:  9  40  200"
        };
        
        Assert.AreEqual(Int128.Parse("71503"), new Day(lines).Execute(Part.Part2));
    }
}