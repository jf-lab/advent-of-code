using static _2023.Part;

namespace _2023.day06;

public class Day(List<string> lines)
{
    public Int128 Execute(Part part)
    {
        return part switch
        {
            Part1 => ExecuteParts(Part1),
            Part2 => ExecuteParts(Part2),
            _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
        };
    }

    private static List<string> ParseLine(string line, Part part)
    {
        var splitRaw = line.Split(":")[1];
        
        return part switch
        {
            Part1 => line.Split(" ").Where(s => !s.Equals("")).ToList(),
            Part2 => new List<string>
            {
                splitRaw.Replace(" ", "")
            },
            _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
        };
    }

    private Int128 ExecuteParts(Part part)
    {
        List<(Int128, Int128)> races = new();
        List<string> timings = new();

        foreach (var line in lines)
        {
            if (line.StartsWith("Time"))
            {
                timings = ParseLine(line, part);
            }

            if (line.StartsWith("Distance"))
            {
                var distances = ParseLine(line, part);

                var offset = part switch
                {
                    Part1 => 1,
                    Part2 => 0,
                    _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
                };

                for (var i = offset; i < distances.Count; i++)
                {
                    var tuple = (Int128.Parse(timings[i]), Int128.Parse(distances[i]));
                    races.Add(tuple);
                }
            }
        }

        List<Int128> brokenRecords = new();

        foreach (var (time, recordDistance) in races)
        {
            var brokenRecordForOne = 0;
            for (Int128 buttonPressed = 0; buttonPressed <= time; buttonPressed++)
            {
                var timeLeft = time - buttonPressed;
                var newDistance = timeLeft * buttonPressed;
                if (newDistance > recordDistance)
                {
                    brokenRecordForOne += 1;
                }
            }

            brokenRecords.Add(brokenRecordForOne);
        }

        return brokenRecords.Aggregate(Int128.One, (current, brokenRecord) => current * brokenRecord);
    }
}