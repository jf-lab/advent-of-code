﻿using System.Text.RegularExpressions;

namespace _2023.day01;

public class Day01
{
    private readonly SortedDictionary<string, string> _numberMapper = new()
    {
        { "zero", "0" },
        { "one", "1" },
        { "two", "2" },
        { "three", "3" },
        { "four", "4" },
        { "five", "5" },
        { "six", "6" },
        { "seven", "7" },
        { "eight", "8" },
        { "nine", "9" }
    };

    public enum Part
    {
        Part1,
        Part2
    }

    public int SumOfCalibrations { get; }

    public Day01(List<string> lines, Part part)
    {
        foreach (var line in lines)
        {
            string calibrationNumberString;
            
            switch (part)
            {
                case Part.Part1:
                    calibrationNumberString = ParseLine(line);
                    break;
                case Part.Part2:
                    calibrationNumberString = CleanData(line);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(part), part, null);
            }

            if (Int32.TryParse(calibrationNumberString, out int calibrationNumber))
            {
                SumOfCalibrations += calibrationNumber;
            }
        }
    }

    private string ParseLine(string line)
    {
        var digits = Regex.Replace(line, @"\D", "");


        var first = digits.First();
        var last = digits.Last();
        var calibrationNumberString = new string(new[]
        {
            first, last
        });
        return calibrationNumberString;
    }

    private string CleanData(string line)
    {
        var foundStrings = new SortedDictionary<int, string>();

        foreach (var (textNumber, numberValue) in _numberMapper)
        {
            var (posBeginning, posLast, result) = ParseNumber(line, textNumber, numberValue);
            
            if (posBeginning >= 0)
            {
                foundStrings.Add(posBeginning, result);
                foundStrings.TryAdd(posLast, result);
            }
        }

        if (foundStrings.Count == 0)
        {
            return ParseLine(line);
        }

        var (_, firstFoundString) = foundStrings.First();
        var (_, lastFoundString) = foundStrings.Last();
        return ParseLine(firstFoundString + lastFoundString);
    }

    (int begin, int end, string result) ParseNumber(string line, string needle, string number)
    {
        var position = line.IndexOf(needle, StringComparison.Ordinal);
        var lastPosition = line.LastIndexOf(needle, StringComparison.Ordinal);
        return (position, lastPosition, line.Replace(needle, number));
    }
}