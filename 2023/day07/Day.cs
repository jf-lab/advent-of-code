using static _2023.Part;

namespace _2023.day07;

public class Day(IEnumerable<string> lines) 
{
    public int Execute(Part part)
    {
        var allGames = lines.Select(line => new Game(part, line)).ToList();

        allGames.Sort();

        return allGames.Select((t, i) => (i + 1) * t.Bid).Sum();
    }

    public enum SpecialSet
    {
        HighCard,
        OnePair,
        TwoPair,
        ThreeOfAKind,
        FullHouse,
        FourOfAKind,
        FiveOfAKind
    }

    public record Hand : IComparable
    {
        private readonly Part _part;

        private readonly Dictionary<char, int> _cardsPart1 = new()
        {
            { '2', 2 },
            { '3', 3 },
            { '4', 4 },
            { '5', 5 },
            { '6', 6 },
            { '7', 7 },
            { '8', 8 },
            { '9', 9 },
            { 'T', 10 },
            { 'J', 11 },
            { 'Q', 12 },
            { 'K', 13 },
            { 'A', 14 }
        };

        private readonly Dictionary<char, int> _cardsPart2 = new()
        {
            { 'J', 1 },
            { '2', 2 },
            { '3', 3 },
            { '4', 4 },
            { '5', 5 },
            { '6', 6 },
            { '7', 7 },
            { '8', 8 },
            { '9', 9 },
            { 'T', 10 },
            { 'Q', 12 },
            { 'K', 13 },
            { 'A', 14 }
        };

        private readonly int[] _cardsInHand = new int[5];

        public SpecialSet Set { get; }

        public Hand(Part part, string input)
        {
            _part = part;

            var cards = part switch
            {
                Part1 => _cardsPart1,
                Part2 => _cardsPart2,
                _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
            };

            var counter = 0;
            foreach (var c in input)
            {
                _cardsInHand[counter] = cards[c];
                counter++;
            }

            Set = ResolveSet();
        }

        private static SpecialSet ResolveSet(List<int> cardInHandList)
        {
            cardInHandList.Sort();

            var distinctCards = cardInHandList.Distinct().ToList();
            var distinctCardsCount = distinctCards.Count;

            switch (distinctCardsCount)
            {
                case 5:
                    return SpecialSet.HighCard;
                case 1:
                    return SpecialSet.FiveOfAKind;
                case 4:
                    return SpecialSet.OnePair;
                case 3:
                {
                    var maxCountOfSameCards = distinctCards.Select(c => cardInHandList.Count(ih => ih == c)).Max();
                    return maxCountOfSameCards == 3 ? SpecialSet.ThreeOfAKind : SpecialSet.TwoPair;
                }
                case 2:
                {
                    var firstCard = distinctCards[0];
                    var countMatchingCards = cardInHandList.Count(c => c == firstCard);
                    if (countMatchingCards is 4 or 1)
                    {
                        return SpecialSet.FourOfAKind;
                    }

                    return SpecialSet.FullHouse;
                }
                default:
                    return SpecialSet.HighCard;
            }
        }

        private SpecialSet ResolveSet()
        {
            if (_part.Equals(Part1))
            {
                return ResolveSet(_cardsInHand.ToList());
            }

            var onlyJokersInHand = _cardsInHand.Count(c => c == 1) == 5;
            if (onlyJokersInHand)
            {
                return SpecialSet.FiveOfAKind;
            }

            var cardsInHandWithJokers = _cardsInHand.ToList();

            var nonJokerCards = _cardsInHand.ToList();
            nonJokerCards.RemoveAll(c => c == 1);

            var found = SpecialSet.HighCard;

            foreach (var nonJokerCard in nonJokerCards)
            {
                List<int> newCardsInHand = new();
                newCardsInHand.AddRange(cardsInHandWithJokers);
                var newHand = newCardsInHand.Select(c => c == 1 ? nonJokerCard : c).ToList();
                var newSetType = ResolveSet(newHand);
                if ((int)found < (int)newSetType)
                {
                    found = newSetType;
                }
            }

            return found;
        }

        public virtual bool Equals(Hand? other)
        {
            if (ReferenceEquals(null, other)) return false;
            return this.Set.Equals(other.Set) && _cardsInHand.Equals(other._cardsInHand);
        }

        public override int GetHashCode()
        {
            return _cardsInHand.GetHashCode();
        }

        public int CompareTo(object? obj)
        {
            if (obj is not Hand other)
            {
                return -1;
            }

            if (Equals(other))
            {
                return 0;
            }

            var thisType = (int)Set;
            var otherType = (int)other.Set;
            if (thisType != otherType)
            {
                return thisType - otherType;
            }

            for (var i = 0; i < _cardsInHand.Length; i++)
            {
                var difference = _cardsInHand[i] - other._cardsInHand[i];
                if (difference != 0)
                {
                    return difference;
                }
            }

            return 0;
        }
    }

    private class Game : IComparable
    {
        private readonly int _bid;
        private readonly string _handString;

        public int Bid => _bid;
        private Hand Hand { get; }

        public Game(Part part, string line)
        {
            var lineSplit = line.Split(" ");
            _handString = lineSplit[0];
            Hand = new Hand(part, lineSplit[0]);
            _bid = int.Parse(lineSplit[1]);
        }

        public int CompareTo(object? obj)
        {
            if (obj is Game other)
            {
                return Hand.CompareTo(other.Hand);
            }

            return -1;
        }

        public override string ToString()
        {
            return $"{nameof(_bid)}: {_bid}, {nameof(_handString)}: {_handString}, {Hand.Set}";
        }
    }
}