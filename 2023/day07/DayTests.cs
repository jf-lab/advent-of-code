using NUnit.Framework;
using static _2023.Part;

namespace _2023.day07;

public class DayTests
{
    [Test]
    public void ExamplePart1Test()
    {
        var lines = new List<string>
        {
            "32T3K 765",
            "T55J5 684",
            "KK677 28",
            "KTJJT 220",
            "QQQJA 483"
        };

        Day day = new(lines);
        Assert.AreEqual(6440, day.Execute(Part1));
    }

    [Test]
    public void ExamplePart2Test()
    {
        var lines = new List<string>
        {
            "32T3K 765",
            "T55J5 684",
            "KK677 28",
            "KTJJT 220",
            "QQQJA 483"
        };
        Day day = new(lines);
        Assert.AreEqual(5905, day.Execute(Part2));
    }
}