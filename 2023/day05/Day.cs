using static System.Text.RegularExpressions.Regex;
using static _2023.Part;

namespace _2023.day05;

public class Day(List<string> lines)
{
    public record SeedRange(Int128 RangeStart, Int128 RangeEnd)
    {
        public static SeedRange Create(string rangeStart, string rangeLength)
        {
            var start = Int128.Parse(rangeStart);
            var length = Int128.Parse(rangeLength);
            return new SeedRange(start, start + length);
        }
    }

    public record Item(Int128 SourceRangeStart, Int128 DestinationRangeStart, Int128 RangeLength)
    {
        public static Item Create(string line)
        {
            var splitLines = line.Split(" ");
            var source = Int128.Parse(splitLines[0]);
            var destination = Int128.Parse(splitLines[1]);
            var range = Int128.Parse(splitLines[2]);
            return new Item(destination, source, range);
        }

        public bool IsInRange(Int128 number)
        {
            return number >= SourceRangeStart && number < SourceRangeStart + RangeLength;
        }

        public bool IsInRange(SeedRange seedRange)
        {
            return seedRange.RangeEnd > SourceRangeStart &&
                   seedRange.RangeStart < SourceRangeStart + RangeLength;
        }

        public SeedRange ResolveDestination(SeedRange seedRange)
        {
            if (IsInRange(seedRange))
            {
                var start = Int128.Max(seedRange.RangeStart, SourceRangeStart) - SourceRangeStart +
                            DestinationRangeStart;
                var end = Int128.Min(seedRange.RangeEnd, SourceRangeStart + RangeLength) - SourceRangeStart +
                    DestinationRangeStart - 1;

                return new SeedRange(start, end);
            }

            return seedRange;
        }

        public Int128 ResolveDestination(Int128 number)
        {
            if (IsInRange(number))
            {
                return number - SourceRangeStart + DestinationRangeStart;
            }

            return number;
        }
    }


    public Int128 Execute(Part part)
    {
        return part switch
        {
            Part1 => ExecutePart1(),
            Part2 => ExecutePart2(),
            _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
        };
    }

    private Int128 ExecutePart2()
    {
        List<SeedRange> seeds = new();
        Dictionary<string, string> guide = new();
        Dictionary<string, List<Item>> almanac = new();
        var selectedMap = "";

        foreach (var line in lines)
        {
            if (line.Trim().StartsWith("seeds: "))
            {
                var seedsRawRangeData = line.Split(" ").Where(s => IsMatch(s, @"\d+")).ToList();
                for (var i = 0; i < seedsRawRangeData.Count; i += 2)
                {
                    seeds.Add(SeedRange.Create(seedsRawRangeData[i], seedsRawRangeData[i + 1]));
                }
            }
            else if (line.Trim().EndsWith(" map:"))
            {
                var split = line.Replace(" map:", "").Split("-");
                guide.Add(split[0], split[2]);
                almanac.Add(split[0], new List<Item>());
                selectedMap = split[0];
            }
            else if (IsMatch(line, @"\d+ \d+ \d+"))
            {
                var item = Item.Create(line);
                almanac[selectedMap].Add(item);
            }
        }

        var lowestLocationNumber = Int128.MaxValue;

        foreach (var (rangeStart, rangeEnd) in seeds)
        {
            for (var seed = rangeStart; seed < rangeEnd; seed++)
            {
                var selectedAlmanac = "seed";
                var newSeed = seed;

                while (guide.ContainsKey(selectedAlmanac))
                {
                    foreach (var item in almanac[selectedAlmanac])
                    {
                        if (item.IsInRange(newSeed))
                        {
                            newSeed = item.ResolveDestination(newSeed);
                            break;
                        }
                    }

                    selectedAlmanac = guide[selectedAlmanac];

                    if (selectedAlmanac.Equals("location") && newSeed < lowestLocationNumber)
                    {
                        lowestLocationNumber = newSeed;
                    }
                }
            }
        }

        return lowestLocationNumber;
    }

    private Int128 ExecutePart1()
    {
        List<Int128> seeds = new();
        Dictionary<string, string> guide = new();
        Dictionary<string, List<Item>> almanac = new();
        var selectedMap = "";

        foreach (var line in lines)
        {
            if (line.Trim().StartsWith("seeds: "))
            {
                seeds = line.Split(" ").Where(s => IsMatch(s, @"\d+")).Select(Int128.Parse).ToList();
            }
            else if (line.Trim().EndsWith(" map:"))
            {
                var split = line.Replace(" map:", "").Split("-");
                guide.Add(split[0], split[2]);
                almanac.Add(split[0], new List<Item>());
                selectedMap = split[0];
            }
            else if (IsMatch(line, @"\d+ \d+ \d+"))
            {
                var item = Item.Create(line);
                almanac[selectedMap].Add(item);
            }
        }

        var lowestLocationNumber = Int128.MaxValue;

        foreach (var seed in seeds)
        {
            string selectedAlmanac = "seed";
            var newSeed = seed;

            while (guide.ContainsKey(selectedAlmanac))
            {
                foreach (var item in almanac[selectedAlmanac])
                {
                    if (item.IsInRange(newSeed))
                    {
                        newSeed = item.ResolveDestination(newSeed);
                        break;
                    }
                }

                selectedAlmanac = guide[selectedAlmanac];

                if (selectedAlmanac.Equals("location") && newSeed < lowestLocationNumber)
                {
                    lowestLocationNumber = newSeed;
                }
            }
        }


        return lowestLocationNumber;
    }
}