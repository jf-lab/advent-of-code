# [advent of code 2023](https://adventofcode.com/2023) 🎄🎅🧑‍🎄

In 2023 I was thinking of a good reason to invest time to start learn more on C#.
Advent of code starts now, so a good reason to start solving problems in C#. 😉

## my setup

I use a windows laptop (windows 11) and [rider](https://www.jetbrains.com/rider/).
I opted to use the new [.net 8.0 framework][dotnet-8] and [C# version 12][csharp-12].

## my adventures

| day         | implementation        | tests               | adventure                                                                                             | left to improve              |
|-------------|-----------------------|---------------------|-------------------------------------------------------------------------------------------------------|------------------------------|
| [01][day01] | [Day01.cs][day01impl] | [tests][day01tests] | Part2 was a difficult challenge for the first day, we don't start easy.                               |                              | 
| [02][day02] | [Day02.cs][day02impl] | [tests][day02tests] | This day was an easy one, not too complicated.                                                        |                              | 
| [03][day03] | [Day03.cs][day03impl] | [tests][day03tests] | The parts near the edges were not part of my solution, so it took more time than necessary.           |                              |
| [04][day04] |                       |                     | My computer crashed, maybe I can recover the data later                                               |                              |
| [05][day05] | [Day.cs][day05impl]   | [tests][day05tests] | Unable to parse data of part 2, example works by the way. Later decided to brute force it.            | solve part 2 + remove Int128 |
| [06][day06] | [Day.cs][day06impl]   | [tests][day06tests] | Easy one today, where is the catch? I brute forced Part2, by increasing the type to Int128.           | change Int128 to int         |
| [07][day07] | [Day.cs][day07impl]   | [tests][day07tests] | The riddle of today was very enjoyable. Learned a lot of new C# syntax.                               |                              |
| [08][day08] | [Day.cs][day08impl]   | [tests][day08tests] | Part 1 was easy, I gave up on part 2 and will try to solve it later. It can be solved with lcm! 😏    |                              |
| [09][day09] | [Day.cs][day09impl]   | [tests][day09tests] | This riddle was easy to do. I did the most in this exercise to make the code as beautiful as I could. |                              |
| [10][day10] | [Day.cs][day10impl]   | [tests][day10tests] | Only completed part 1, it took 4 hours to process the data. Should be improved. 😂                    | solve part 2                 |
| [11][day11] | [Day.cs][day11impl]   | [tests][day11tests] | Surprising simple exercise, it was possible to add part 2 after simple refactoring.                   |                              |

[//]: # (| [11][day11] | [Day.cs][day11impl]   | [tests][day11tests] |                                                                                                       |                              |)


[day01]: https://adventofcode.com/2023/day/1

[day01impl]: ./day01/Day01.cs

[day01tests]: ./day01/Day01Tests.cs

[day02]: https://adventofcode.com/2023/day/2

[day02impl]: ./day02/Day02.cs

[day02tests]: ./day02/Day02Tests.cs

[day03]: https://adventofcode.com/2023/day/3

[day03impl]: ./day03/Day03.cs

[day03tests]: ./day03/Day03Tests.cs

[day04]: https://adventofcode.com/2023/day/4

[day05]: https://adventofcode.com/2023/day/5

[day05impl]: ./day05/Day.cs

[day05tests]: ./day05/DayTests.cs

[day06]: https://adventofcode.com/2023/day/6

[day06impl]: ./day06/Day.cs

[day06tests]: ./day06/DayTests.cs

[day07]: https://adventofcode.com/2023/day/7

[day07impl]: ./day07/Day.cs

[day07tests]: ./day07/DayTests.cs

[day08]: https://adventofcode.com/2023/day/8

[day08impl]: ./day08/Day.cs

[day08tests]: ./day08/DayTests.cs

[day09]: https://adventofcode.com/2023/day/9

[day09impl]: ./day09/Day.cs

[day09tests]: ./day09/DayTests.cs

[day10]: https://adventofcode.com/2023/day/10

[day10impl]: ./day10/Day.cs

[day10tests]: ./day10/DayTests.cs

[day11]: https://adventofcode.com/2023/day/11

[day11impl]: ./day11/Day.cs

[day11tests]: ./day11/DayTests.cs

[day12]: https://adventofcode.com/2023/day/12

[day13]: https://adventofcode.com/2023/day/13

[day14]: https://adventofcode.com/2023/day/14

[day15]: https://adventofcode.com/2023/day/15

[day16]: https://adventofcode.com/2023/day/16

[day17]: https://adventofcode.com/2023/day/17

[day18]: https://adventofcode.com/2023/day/18

[day19]: https://adventofcode.com/2023/day/19

[csharp-12]: https://learn.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-12

[dotnet-8]: https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-8