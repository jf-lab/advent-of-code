using NUnit.Framework;
using static _2023.Part;

namespace _2023.day08;

public class DayTests
{
    [Test]
    public void Part1Example1Test()
    {
        var lines = new List<string>
        {
            "RL",
            "",
            "AAA = (BBB, CCC)",
            "BBB = (DDD, EEE)",
            "CCC = (ZZZ, GGG)",
            "DDD = (DDD, DDD)",
            "EEE = (EEE, EEE)",
            "GGG = (GGG, GGG)",
            "ZZZ = (ZZZ, ZZZ)"
        };
        Day day = new(lines);
        Assert.AreEqual(2, day.Execute(Part1));
    }

    [Test]
    public void Part1Example2Test()
    {
        var lines = new List<string>
        {
            "LLR",
            "",
            "AAA = (BBB, BBB)",
            "BBB = (AAA, ZZZ)",
            "ZZZ = (ZZZ, ZZZ)"
        };
        Day day = new(lines);
        Assert.AreEqual(6, day.Execute(Part1));
    }

    [Test]
    public void Part2Example1Test()
    {
        var lines = new List<string>
        {
            "LR",
            "",
            "11A = (11B, XXX)",
            "11B = (XXX, 11Z)",
            "11Z = (11B, XXX)",
            "22A = (22B, XXX)",
            "22B = (22C, 22C)",
            "22C = (22Z, 22Z)",
            "22Z = (22B, 22B)",
            "XXX = (XXX, XXX)"
        };
        Day day = new(lines);
        Assert.AreEqual(6, day.Execute(Part2));
    }
}