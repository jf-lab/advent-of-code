using System.ComponentModel;

namespace _2023.day08;

public class Day(List<string> lines)
{
    public long Execute(Part part)
    {
        return part switch
        {
            Part.Part1 => ExecutePart1(),
            Part.Part2 => ExecutePart2(),
            _ => throw new ArgumentOutOfRangeException(nameof(part), part, null)
        };
    }

    private long ExecutePart2()
    {
        Dictionary<string, (string, string)> map = new();
        bool directionsFound = false;
        var directions = "";
        List<string> startNodes = new();

        foreach (var line in lines)
        {
            if (!directionsFound)
            {
                directions = line;
                directionsFound = true;
                continue;
            }

            if (line is not "")
            {
                var splitKeyAndTuple = line.Split(" = ");
                var key = splitKeyAndTuple[0];
                var tuple = splitKeyAndTuple[1].Replace("(", "").Replace(")", "");
                var left = tuple.Split(", ")[0];
                var right = tuple.Split(", ")[1];
                map.Add(key, (left, right));
                if (key.EndsWith('A'))
                {
                    startNodes.Add(key);
                }
            }
        }

        var startNodesInString = string.Join("-", startNodes);
        Console.WriteLine($"These are the start nodes: {startNodesInString}");

        List<long> resolveFirstCycle = [];

        foreach (var startNode in startNodes)
        {
            bool notAtTheEnd = true;
            var startIndex = 0;
            var maxIndex = directions.Length;
            var index = 0;
            var steps = 0;

            var numberOfZtoBeFound = 5;

            var node = startNode;

            while (notAtTheEnd)
            {
                if (index == maxIndex)
                {
                    index = startIndex;
                }

                var direction = directions[index];

                var (left, right) = map[node];

                var newNode = direction switch
                {
                    'R' => right,
                    'L' => left,
                    _ => throw new ArgumentOutOfRangeException()
                };

                node = newNode;
                index++;
                steps++;

                if (newNode[2] == 'Z')
                {
                    Console.WriteLine($"{startNode} - {node} --- {steps}");
                    resolveFirstCycle.Add(steps);
                    notAtTheEnd = false;
                }
            }
        }

        return MathUtil.LCM(resolveFirstCycle);
    }

    private int ExecutePart1()
    {
        Dictionary<string, (string, string)> map = new();
        bool directionsFound = false;
        var directions = "";
        foreach (var line in lines)
        {
            if (!directionsFound)
            {
                directions = line;
                directionsFound = true;
                continue;
            }

            if (line is not "")
            {
                var splitKeyAndTuple = line.Split(" = ");
                var key = splitKeyAndTuple[0];
                var tuple = splitKeyAndTuple[1].Replace("(", "").Replace(")", "");
                var left = tuple.Split(", ")[0];
                var right = tuple.Split(", ")[1];
                map.Add(key, (left, right));
            }
        }

        bool notAtTheEnd = true;
        var startIndex = 0;
        var maxIndex = directions.Length;
        var node = "AAA";
        var index = 0;
        var steps = 0;

        while (notAtTheEnd)
        {
            if (index == maxIndex)
            {
                index = startIndex;
            }

            var (left, right) = map[node];

            var direction = directions[index];

            node = direction switch
            {
                'R' => right,
                'L' => left,
                _ => throw new ArgumentOutOfRangeException()
            };

            if (node.Equals("ZZZ"))
            {
                notAtTheEnd = false;
            }

            index++;
            steps++;
        }

        return steps;
    }
}