<#

    .SYNOPSIS
    Creates a fresh new namespace with Day.cs and DayTests.cs pupulated.


    .PARAMETER Day
    Input the day number. Prefix with a zero when it is smaller than 10. 


    .EXAMPLE
    PS> .\CreateNewDay.ps1 -Day 01

    .EXAMPLE
    PS> .\CreateNewDay.ps1 -Day 20

#>

[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [String]
    $Day
)

$template = "dayxx"
$newName = "day$Day"

Copy-Item -Path "dayxx" -Recurse -Destination "day$Day"

Set-Location -Path $newName

$null = New-Item input.txt

$content = [System.IO.File]::ReadAllText("$newName\Day.cs").Replace($template, $newName)
[System.IO.File]::WriteAllText("$newName\Day.cs", $content)

$content = [System.IO.File]::ReadAllText("$newName\DayTests.cs").Replace($template, $newName)
[System.IO.File]::WriteAllText("$newName\DayTests.cs", $content)

Set-Location -Path ..
