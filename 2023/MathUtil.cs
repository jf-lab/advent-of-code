namespace _2023;

public record MathUtil()
{
    /// <summary>
    /// This function calculates the Least common multiple.
    /// This is the smallest integer that is both visible by the numbers as input.
    /// </summary>
    /// <param name="numbers">array with numbers to find the lcm</param>
    /// <param name="b">second number</param>
    /// <code>
    /// var lcm = MathUtil.LCM([12, 8, 4]);
    /// // lcm will return 24 in this case
    /// </code>
    /// <returns>least common multiple</returns>
    public static long LCM(List<long> numbers)
    {
        var numberList = numbers.ToList();
        if (numberList.Count < 2)
        {
            throw new ArgumentException("The LCM method needs at least 2 numbers");
        }
        return numberList.Aggregate(lcm);
    }
    
    /// <summary>
    /// This function calculates the Least common multiple.
    /// This is the smallest integer that is both visible by the numbers as input.
    /// </summary>
    /// <param name="a">first number</param>
    /// <param name="b">second number</param>
    /// <code>
    /// var lcm = MathUtil.lcm(12, 8);
    /// // lcm will return 24 in this case
    /// </code>
    /// <returns>least common multiple</returns>
    private static long lcm(long a, long b)
    {
        return Math.Abs(a * b) / gcm(a, b);
    }
    
    /// <summary>
    /// This function calculates the greatest common divisor, this finds the largest number which
    /// can be used to divide the two arguments.
    /// See for more information: https://en.wikipedia.org/wiki/Greatest_common_divisor
    /// </summary>
    /// <param name="a">first number</param>
    /// <param name="b">second number</param>
    /// <code>
    /// var gcm = MathUtil.gcm(12, 8);
    /// // gcm will return 4 in this case
    /// </code>
    /// <returns>greatest common divisor for a and b</returns>
    private static long gcm(long a, long b)
    {
        return b == 0 ? a : gcm(b, a % b);
    }
}