using NUnit.Framework;
using static _2023.Part;

namespace _2023.day09;

public class DayTests
{
    [Test]
    public void Part1ExampleTest()
    {
        var lines = new List<string>
        {
            "0 3 6 9 12 15",
            "1 3 6 10 15 21",
            "10 13 16 21 30 45"
        };
        Day day = new(lines);
        Assert.AreEqual(114, day.Execute(Part1));
    }

    [Test]
    public void Part2ExampleTest()
    {
        var lines = new List<string>
        {
            "0 3 6 9 12 15",
            "1 3 6 10 15 21",
            "10 13 16 21 30 45"
        };
        Day day = new(lines);
        Assert.AreEqual(2, day.Execute(Part2));
    }

    private static void AssertAline(int expected, Part part, string input)
    {
        var numbers = input.Split(" ").Select(int.Parse).ToList();
        Assert.AreEqual(expected, Day.ProcessOneLine(true, part, numbers));
        Console.WriteLine($"Extrapolated number: {expected}");
    }

    [Test]
    public void ProcessOneLineFirstLineInExampleTest()
    {
        AssertAline(18, Part1, "0 3 6 9 12 15");
    }

    [Test]
    public void ProcessOneLineLastLineInExampleTest()
    {
        AssertAline(68, Part1, "10 13 16 21 30 45");
    }

    [Test]
    public void ProcessOneLineLastLineInExamplePart2Test()
    {
        AssertAline(5, Part2, "10 13 16 21 30 45");
    }

    [Test]
    public void ProcessLineWithNegativeValuesFromInputTest()
    {
        AssertAline(-54, Part1, "9 6 3 0 -3 -6 -9 -12 -15 -18 -21 -24 -27 -30 -33 -36 -39 -42 -45 -48 -51");
    }

    [Test]
    public void ProcessLastLineLastLineInExampleTest()
    {
        AssertAline(-971, Part1,
            "-5 -11 -21 -35 -53 -75 -101 -131 -165 -203 -245 -291 -341 -395 -453 -515 -581 -651 -725 -803 -885");
    }

    [Test]
    public void ProcessRandomLineInInputTest()
    {
        AssertAline(-78431, Part1,
            "-3 -8 -18 -23 7 125 405 935 1805 3090 4828 6993 9463 11983 14123 15231 14381 10316 1386 -14519 -40041");
    }

    [Test]
    public void Input02Text()
    {
        AssertAline(-54, Part1, "9 6 3 0 -3 -6 -9 -12 -15 -18 -21 -24 -27 -30 -33 -36 -39 -42 -45 -48 -51");
    }

    [Test]
    public void Input03Text()
    {
        AssertAline(14007565, Part1,
            "8 23 43 64 87 126 230 542 1426 3703 9058 20729 44688 91697 180910 346222 647728 1193456 2183213 4001351 7416521");
    }
}