using static System.Environment;
using static _2023.Part;

namespace _2023.day09;

public class Day(IEnumerable<string> lines)
{
    public int Execute(Part part)
    {
        return lines.Select(line => line.Split(" ").Select(int.Parse).ToList())
            .Select(numbers => ProcessOneLine(false, part, numbers))
            .Sum();
    }

    private static void PrintIntList(bool print, IEnumerable<int> line)
    {
        if (!print)
        {
            return;
        }

        Console.WriteLine(string.Join(" ", line));
    }

    private static void UpdateNumberList(ICollection<int> numberDiff, Part part, int itemPart1, int itemPart2)
    {
        switch (part)
        {
            case Part1:
                numberDiff.Add(itemPart1);
                break;
            case Part2:
                numberDiff.Add(itemPart2);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(part), part, null);
        }
    }

    public static int ProcessOneLine(bool print, Part part, List<int> line)
    {
        var numberDiff = new List<int>();
        var diffs = line;

        UpdateNumberList(numberDiff, part, line.Last(), line.First());

        PrintIntList(print, line);

        int diff;

        PrintIntList(print, diffs);

        while (!(diffs[0] == 0 && diffs.Distinct().Count() == 1))
        {
            line = diffs;

            diffs = [];
            diff = 0;

            for (var i = 0; i < line.Count - 1; i++)
            {
                diff = line[i + 1] - line[i];
                diffs.Add(diff);
            }

            PrintIntList(print, diffs);

            UpdateNumberList(numberDiff, part, diff, diffs[0]);
        }

        if (part.Equals(Part1))
        {
            return numberDiff.Sum();
        }

        diff = 0;
        for (var i = numberDiff.Count - 1; i >= 0; i--)
        {
            diff = numberDiff[i] - diff;
            if (print)
            {
                Console.Write($"*** {diff}");
            }
        }

        if (print)
        {
            Console.WriteLine($"{diff} {NewLine}");
        }

        return diff;
    }
}