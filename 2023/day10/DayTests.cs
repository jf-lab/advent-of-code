using NUnit.Framework;
using static _2023.Part;

namespace _2023.day10;

public class DayTests
{
    [Test]
    public void Part1Example1ATest()
    {
        var input = """
                    .....
                    .S-7.
                    .|.|.
                    .L-J.
                    .....
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(4, day.Execute());
    }
    
    [Test]
    public void Part1Example1BTest()
    {
        var input = """
                    -L|F7
                    7S-7|
                    L|7||
                    -L-J|
                    L|-JF
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(4, day.Execute());
    }
    
    [Test]
    public void Part1Example2Test()
    {
        var input = """
                    ..F7.
                    .FJ|.
                    SJ.L7
                    |F--J
                    LJ...
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(8, day.Execute());
    }
    


    [Test]
    public void Part2ExampleTest()
    {
        var input = """
                    INPUT
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.AreEqual(1, day.Execute());
    }
    
    [Test]
    public void Part1ResolveCharTest()
    {
        var input = """
                    ..F7.
                    .FJ|.
                    SJ.L7
                    |F--J
                    LJ...
                    """.Split(Environment.NewLine).ToList();

        Day day = new(input, Part1);
        Assert.That(day.ResolveChar(0,0), Is.EqualTo('.'));
        Assert.That(day.ResolveChar(1,1), Is.EqualTo('F'));
        Assert.That(day.ResolveChar(0,2), Is.EqualTo('S'));
        Assert.That(day.ResolveChar(0,2), Is.EqualTo('S'));
        Assert.That(day.ResolveChar(100,100), Is.Null);
    }
}