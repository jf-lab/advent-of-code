namespace _2023.day10;

using AStarNavigator;
using AStarNavigator.Algorithms;
using AStarNavigator.Providers;

// Part 1 takes 4 hours and 45 minutes to run
public class Day(IReadOnlyList<string> lines, Part part) : IBlockedProvider, INeighborProvider
{
    private readonly int _maxX = lines[0].Length;
    private readonly int _maxY = lines.Count;

    public void FindAll(List<Tile> tiles, TileNavigator navigator, Tile startTile)
    {
        var i = 0;
        int max = 0;
        foreach (var t in tiles)
        {
            var path = navigator.Navigate(startTile, t);
            if (path == null)
            {
                Console.WriteLine($"{StringFormat(startTile)} -> {StringFormat(t)} :: no-path");
            }
            else
            {
                var pathTileCount = path.Count();
                Console.WriteLine($"{StringFormat(startTile)} -> {StringFormat(t)} :: {pathTileCount}");

                max = Math.Max(max, pathTileCount);
            }

            // if (i == 2)
            // {
            //     break;
            // }

            i++;
        }

        Console.WriteLine($"Found in {i} iterations");
        Console.WriteLine($"Max path: {max}");
    }

    public int Execute()
    {
        var (startTile, otherTiles) = InputToTiles();

        Console.WriteLine($"Start analyzing {otherTiles.Count} tiles");

        var pieceToAnalyze = otherTiles.Count / 7;

        var navigator = new TileNavigator(this, this, new PythagorasAlgorithm(), new ManhattanHeuristicAlgorithm());

        var part1 = otherTiles.GetRange(0, pieceToAnalyze);
        var part2 = otherTiles.GetRange(pieceToAnalyze, pieceToAnalyze);
        var part3 = otherTiles.GetRange(2 * pieceToAnalyze, pieceToAnalyze);
        var part4 = otherTiles.GetRange(3 * pieceToAnalyze, pieceToAnalyze);
        var part5 = otherTiles.GetRange(4 * pieceToAnalyze, pieceToAnalyze);
        var part6 = otherTiles.GetRange(5 * pieceToAnalyze, pieceToAnalyze);
        var part7 = otherTiles.GetRange(6 * pieceToAnalyze, pieceToAnalyze);
        var part8 = otherTiles.GetRange(7 * pieceToAnalyze, otherTiles.Count() - 7 * pieceToAnalyze);


        var task1 = Task.Factory.StartNew(() => { FindAll(part1, navigator, startTile); });
        var task2 = Task.Factory.StartNew(() => { FindAll(part2, navigator, startTile); });
        var task3 = Task.Factory.StartNew(() => { FindAll(part3, navigator, startTile); });
        var task4 = Task.Factory.StartNew(() => { FindAll(part4, navigator, startTile); });
        var task5 = Task.Factory.StartNew(() => { FindAll(part5, navigator, startTile); });
        var task6 = Task.Factory.StartNew(() => { FindAll(part6, navigator, startTile); });
        var task7 = Task.Factory.StartNew(() => { FindAll(part7, navigator, startTile); });
        var task8 = Task.Factory.StartNew(() => { FindAll(part8, navigator, startTile); });

        Task.WaitAll(task1, task2, task3, task4, task5, task6, task7, task8);
        Console.WriteLine("Done!");
        return 123;
    }

    private string StringFormat(Tile tile)
    {
        return $"({tile.X}, {tile.Y})";
    }

    public char? ResolveChar(double x, double y)
    {
        if (x < _maxX && y < _maxY && x >= 0 && y >= 0)
        {
            return lines[(int)y][(int)x];
        }

        return null;
    }

    private (Tile startTile, List<Tile> otherTiles) InputToTiles()
    {
        var startTile = new Tile();
        var otherTiles = new List<Tile>();

        for (var y = 0; y < lines.Count; y++)
        {
            for (var x = 0; x < lines[y].Length; x++)
            {
                var c = lines[y][x];
                var tile = new Tile(x, y);
                var hasNeighbours = GetNeighbors(tile).Any();
                // var hasNeighbours = true;
                if (c == 'S')
                {
                    startTile = tile;
                }
                else if (!IsBlocked(x, y) && hasNeighbours)
                {
                    otherTiles.Add(tile);
                }
            }
        }

        return (startTile, otherTiles);
    }

    public bool IsBlocked(Tile tile)
    {
        return IsBlocked(tile.X, tile.Y);
    }

    private bool IsBlocked(double x, double y)
    {
        var c = ResolveChar(x, y);
        switch (c)
        {
            case null:
            case '.':
                return true;
            default:
                return false;
        }
    }

    public IEnumerable<Tile> GetNeighbors(Tile tile)
    {
        var x = tile.X;
        var y = tile.Y;

        var currentChar = ResolveChar(x, y);

        var upBlocked = IsBlocked(x, y - 1);
        var downBlocked = IsBlocked(x, y + 1);
        var leftBlocked = IsBlocked(x - 1, y);
        var rightBlocked = IsBlocked(x + 1, y);

        var canUp = !upBlocked &&
                    currentChar is 'S' or '|' or 'L' or 'J' &&
                    ResolveChar(x, y - 1) is '|' or 'F' or '7';

        var canDown = !downBlocked &&
                      currentChar is 'S' or '|' or 'F' or '7' &&
                      ResolveChar(x, y + 1) is '|' or 'L' or 'J';

        var canLeft = !leftBlocked &&
                      currentChar is 'S' or '-' or 'J' or '7' &&
                      ResolveChar(x - 1, y) is '-' or 'L' or 'F';

        var canRight = !rightBlocked &&
                       currentChar is 'S' or '-' or 'L' or 'F' &&
                       ResolveChar(x + 1, y) is '-' or 'J' or '7';

        List<Tile> neighbours = [];
        if (canUp)
        {
            neighbours.Add(new Tile(x, y - 1));
        }

        if (canDown)
        {
            neighbours.Add(new Tile(x, y + 1));
        }

        if (canLeft)
        {
            neighbours.Add(new Tile(x - 1, y));
        }

        if (canRight)
        {
            neighbours.Add(new Tile(x + 1, y));
        }

        return neighbours;
    }
}