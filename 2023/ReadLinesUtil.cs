﻿namespace _2023;

using System;
using System.Collections.Generic;
using System.IO;

public class ReadLinesUtil
{
    public static List<string> ReadAllLines(string filePath)
    {
        var solution_root_dir = Environment.CurrentDirectory.Split(@"bin\").First();
        
        List<string> lines = new List<string>();

        try
        {
            // Open the file with a StreamReader
            using StreamReader reader = new StreamReader(solution_root_dir + filePath);
            // Read all lines and add them to the list
            while (!reader.EndOfStream)
            {
                string? line = reader.ReadLine();
                if (line != null)
                {
                    lines.Add(line);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }

        return lines;
    }
}