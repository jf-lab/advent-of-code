# [advent of code](https://adventofcode.com) 🎄🎅🧑‍🎄

This repository contains my adventures with advent of code.

| language        | my solutions    |
|:----------------|:----------------|
| [🦀][rust-lang] | [2022](./2022/) |
| [C#][csharp]    | [2023](./2023/) |
| [🦀][rust-lang] | [2024](./2024/) |

[rust-lang]: https://www.rust-lang.org

[csharp]: https://dotnet.microsoft.com/en-us/languages/csharp
